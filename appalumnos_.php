<!DOCTYPE html>
<html lang="es">
<?Php
$novedades=base64_encode('novedadesbb');
$option=base64_encode('novedadesbb');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!--	<link rel="stylesheet" href="<?Php echo DEFAULT_LAYOUT;?>/css/lista-desplegable.css">-->
	<link rel="stylesheet" href="style_tabs_acordeon_alumnos - Copy.css">
	<script src="js/index.js"></script>

<!-- Start single page header -->
  <section id="single-page-header-novedades">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Novedades Bb / APP Alumnos</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active"><a href="novedades_bb.php">Novedades Bb</a></li>
				<li class="active">APP Alumnos</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- End single page header -->

<!-- Inicio del contenido de la página-->
  <section id="contact">
     <div class="container">
       <div class="row">
		   <div class="col-xs-12 col-md-4 div-app-facilitadores">
		   		<img src="templates/frontend/images/novedades-blackboard/icono-alumnos.png" style="height:auto; width: 100px;" class="icono-facilitadores" alt="App Facilitadores">
				<p class="titulo-app-fac">APP <br>Alumnos</p>
		 </div>
		   <div class="col-md-2"></div>
         <div class="col-xs-12 col-md-6">
           <div class="title-area title-area-borde">
				 <h1 class="title">Novedades</h1>
				 <h2 class="subtitulo">Plataforma Instruccional Institucional</h2>
			   <center>
			   		<img src="templates/frontend/images/novedades-blackboard/logotipo-blackboard.png" style="height:auto; width: 399px;" class="logo-blackboard" alt="Blackboard">
			   </center>
            </div>
         </div>


       </div>

		<div class="row">
         <div class="col-xs-12 col-md-12">
				<div class="naccs">
				  <div class="grid">
				   <div class="gc col-xs-12 col-md-4">
				    <div class="menu alumnosc">
				     <div ><span class="light"></span><span>Actividades</span></div>
				     <div><span class="light"></span><span>Cursos</span></div>
				     <div><span class="light"></span><span>Foros</span></div>
						<div><span class="light"></span><span>Notificaciones</span></div>
						<div><span class="light"></span><span>Actividades</span></div>
						<div><span class="light"></span><span>Exámenes</span></div>
						<div><span class="light"></span><span>Calificaciones</span></div>
						<div><span class="light"></span><span>Collaborate</span></div>
				    </div>
				   </div>
					  <div class="col-md-2"></div>
				   <div class="gc col-xs-12 col-md-6">
				    <ul class="nacc">
				     <li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido alumnos-p">Contar con tu flujo de actividades.</p>
				       		<img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
				      </div>
				     </li>
				     <li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido alumnos-p">Ver la lista de cursos.</p>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
				      </div>
				     </li>
				     <li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido alumnos-p">Participar en foros.</p>
				        <video autoplay loop>
				          <source src="templates/frontend/videos/facilitador-instalacion-android_13.mp4" type="video/mp4">
				        </video>
				      </div>
				     </li>
						<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido alumnos-p">Revisar notificaciones de fechas de vencimiento de pruebas y actividades.</p>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
				      </div>
				     </li>
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido alumnos-p">Enviar actividades.</p>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
				      </div>
				     </li>
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido alumnos-p">Responder exámenes.</p>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
				      </div>
				     </li>
						<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido alumnos-p">Ver tus calificaciones y comentarios.</span></p>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
				      </div>
				     </li>
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido alumnos-p">Interactuar con tu clase en<br><span>Blackboard Collaborate.</span></p>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
				      </div>
				     </li>
				    </ul>
				   </div>
				  </div>
				 </div>
         </div>
       </div>

     </div>
  </section>

<!-- Final del contenido de la página-->
      <?Php
require_once 'footer/footer_v1.php';
?>


</body>
</html>
