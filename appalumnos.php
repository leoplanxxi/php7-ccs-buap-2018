<!DOCTYPE html>
<html lang="es">
<?Php
$novedades=base64_encode('novedadesbb');
$option=base64_encode('novedadesbb');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';


?>
<!--	<link rel="stylesheet" href="<?Php echo DEFAULT_LAYOUT;?>/css/lista-desplegable.css">-->
	<link rel="stylesheet" href="style_tabs_acordeon_alumnos.css">
	<script src="js/index.js"></script>
<script src='js/zoom/jquery.zoom.js'></script>
<script>
	$(document).ready(function(){
		$('#ex1').zoom();
		$('#ex2').zoom();
		$('#ex3').zoom();
		$('#ex4').zoom();
		$('#ex5').zoom();
		$('#ex6').zoom();
		$('#ex7').zoom();

	});
</script>
<!-- Start single page header -->
  <section id="single-page-header-novedades">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Novedades Bb / APP Alumnos</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active"><a href="novedades-blackboard.php">Novedades Bb</a></li>
				<li class="active">APP Alumnos</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- End single page header -->

<!-- Inicio del contenido de la página-->
  <section id="contact">
     <div class="container">
       <div class="row">
		   <div class="col-xs-12 col-md-4 div-app-facilitadores">

		   		<img src="templates/frontend/images/novedades-blackboard/icono-alumnos.png" class="icono-facilitadores" alt="App Facilitadores">
				<p class="titulo-app-fac">APP <br>Alumnos</p>
		 </div>
		   <div class="col-md-2"></div>
         <div class="col-xs-12 col-md-6">
			 <a href="novedades-blackboard.php">
           <div class="title-area title-area-borde">
				 <h1 class="title">Novedades</h1>
				 <h2 class="subtitulo">Plataforma Instruccional Institucional</h2>
			   <center>
			   		<img src="templates/frontend/images/novedades-blackboard/logo-blackboard-mobile.png" style="height:auto;" class="logo-blackboard" alt="Blackboard">
			   </center>
            </div>
			</a>
         </div>


       </div>

		<div class="row">
         <div class="col-xs-12 col-md-12">
				<div class="naccs lista-opciones">
				  <div class="grid">
				   <div class="gc col-xs-12 col-md-4 col-lg-4">
				    <div class="menu">
				     <div class="active"><span class="light"></span><span>Actividades</span></div>
				     <div><span class="light"></span><span>Cursos</span></div>
				     <div><span class="light"></span><span>Foros</span></div>
						<div><span class="light"></span><span>Notificaciones</span></div>
						<div><span class="light"></span><span>Actividades</span></div>
						<div><span class="light"></span><span>Exámenes</span></div>
						<div><span class="light"></span><span>Calificaciones</span></div>
						<div class="menu-collaborate"><span class="light menu-collaborate"></span><span>Collaborate</span></div>
				    </div>
				   </div>
				<div class="col-md-1 col-lg-1"></div>
				   <div class="gc col-xs-12 col-md-7 col-lg-7">
				    <ul class="nacc" style="height: 880px">
				     <li class="active">
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Contar con el flujo de <span class="t_azul_fuerte_t">Actividades</span>.</p>
						  <p class="novedades-texto-contenido int">Puedes observar el historial de Actividades recientes que has realizado dentro de tus cursos.</p>
						  <iframe class="iframe-alumnos" width="535px" height="600px" src="https://www.youtube.com/embed/yTReOgn55oI" frameborder="0" gesture="media" allowfullscreen></iframe>

				      </div>
				     </li>
				     <li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Ver la lista de <span class="t_azul_fuerte_t">Cursos</span>.</p>
						  <p class="novedades-texto-contenido int">Observarás el listado de cursos a los cuales te encuentras inscrito y podrás navegar en cada uno de ellos.</p>
							<iframe class="iframe-alumnos" width="535px" height="600px" src="https://www.youtube.com/embed/oJDVZRiYvQs" frameborder="0" gesture="media" allowfullscreen></iframe>
				      </div>
				     </li>
						<li>
							<div class="contenedor-acordeon">
								<p class="novedades-texto-contenido titulo-acordeon">Participar en <span class="t_azul_fuerte_t">Foros</span>.</p>
								<p class="novedades-texto-contenido int">Esta herramienta te ayuda a interactuar en un tema de discusión.</p>
								<iframe class="iframe-alumnos" width="535px" height="600px" src="https://www.youtube.com/embed/kl1yRiIv7nE" frameborder="0" gesture="media" allowfullscreen></iframe>
							</div>
						</li>
				     <!--<li>

						 <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Participar en debates</p>
							 <iframe width="100%" height="auto" src="https://www.youtube.com/embed/mVdg678ZAC8" frameborder="0" gesture="media" allowfullscreen></iframe>
				      </div>
				     </li>-->
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Revisar <span class="t_azul_fuerte_t">notificaciones</span></p>
						  <p class="novedades-texto-contenido int">Puedes revisar notificaciones de fechas de vencimiento de pruebas y actividades.</p>
							<span class='zoom' id='ex3'>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
						 </span>
				      </div>
				     </li>
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Enviar <span class="t_azul_fuerte_t">Actividades</span>.</p>
						  <p class="novedades-texto-contenido int">Te guiamos para enviar tus actividades en cualquier curso disponible.</p>
							<iframe class="iframe-alumnos" width="535px" height="600px" src="https://www.youtube.com/embed/Da-XQMVlM_c" frameborder="0" gesture="media" allowfullscreen></iframe>
				      </div>
				     </li>
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Responder <span class="t_azul_fuerte_t">Exámenes</span>.</span></p>
					   <p class="novedades-texto-contenido int">Te mostramos fácilmente cómo contestar Evaluaciones dentro de un curso en la Plataforma.</p>
							<iframe class="iframe-alumnos" width="535px" height="600px" src="https://www.youtube.com/embed/gs4YfdMI7kM" frameborder="0" gesture="media" allowfullscreen></iframe>
				      </div>
				     </li>
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Ver tus <span class="t_azul_fuerte_t">calificaciones</span> y comentarios.</span></p>
			 				<p class="novedades-texto-contenido int">Puedes visualizar tus calificaciones y retroalimentaciones de tu <span class="t_azul_obscuro">Facilitador(a)</span>.</p>
							<iframe class="iframe-alumnos" width="535px" height="600px" src="https://www.youtube.com/embed/2x5pCb9pf7U" frameborder="0" gesture="media" allowfullscreen></iframe>
				      </div>
				     </li>
					<li>
				      <div class="contenedor-acordeon bg_collaborate">
						  <p class="novedades-texto-contenido titulo-acordeon texto-bbcollaborate">Interactuar con tu clase en<br><span>Blackboard Collaborate Ultra.</span></p>
						  <p class="novedades-texto-contenido int"><span class="t_morado_obscuro">Collaborate</span> con la experiencia <span class="t_morado_obscuro">Ultra</span> para conferencias web sincrónicas. Puedes ingresar a clases virtuales en las que tu <span class="t_azul_obscuro">Facilitador(a)</span> te puede compartir archivos y vídeo.</p>
							<iframe class="iframe-alumnos" width="535px" height="600px" src="https://www.youtube.com/embed/RBA62RO_Qms" frameborder="0" gesture="media" allowfullscreen></iframe>
				      </div>
				     </li>
				    </ul>
				   </div>
				  </div>
				 </div>
         </div>
       </div>

     </div>
  </section>

<!-- Final del contenido de la página-->
      <?Php
require_once 'footer/footer_v1.php';
?>


</body>
</html>
