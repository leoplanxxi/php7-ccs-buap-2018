<!DOCTYPE html>
<html lang="en">
<?Php
$faq=base64_encode('faq');
$option=base64_encode('faq');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!-- Start single page header -->
  <section id="single-page-header-faq">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Preguntas frecuentes</h2>
              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>-->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active">Preguntas frecuentes</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <!-- Start Pricing table -->
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Categor&iacute;as</h2>
            <span class="line"></span>
            <p>Haz clic en la categor&iacute;a correspondiente</p>
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content">
            <div class="row">
              <!-- Start single team member -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                 <div class="team-member-img">
                   <img src="<?Php echo DEFAULT_LAYOUT;?>/images/moodle.png" alt="team member img">
                 </div>
                 <div class="team-member-name">
                 	<?Php 
                   $obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
				           $sql='SELECT 
          				   			kbcategoryid, title, totalarticles, categorytype 
          				   		 FROM 
          				   		 	swkbcategories 
          				   		 WHERE  
          				   		 	categorytype=:type';
                   $obj->query($sql);
				          $obj->bind(':type', '1');
				    //$obj->execute();
             	   $categoria=$obj->resultset();
                   $obj->null;
				   //foreach($moodle)
                   ?>
                   <p><?Php echo $categoria[0]['title'];?></p>
                   
                   <span>(<?Php echo $categoria[0]['totalarticles'];?>)</span>
                 </div>
                 	<?Php 
                 	$obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
        					$sql='SELECT 
        						  	kbarticlelinkid, swkbarticles.kbarticleid, subject, swkbarticles.views
        						  FROM 
        						  	swkbarticles, swkbarticlelinks 
        						  WHERE 
        						  	swkbarticlelinks.kbarticleid = swkbarticles.kbarticleid AND
        						  	swkbarticlelinks.linktypeid=:idCat ORDER BY swkbarticles.views DESC LIMIT 5';
        					$obj->query($sql);				  
        				    $obj->bind(':idCat', $categoria[0]['kbcategoryid']);
                     	    $moodle=$obj->resultset();
                            $obj->null;
                         	foreach($moodle as $dMoodle)
        						{
        							?>
        							<a href="question.php?categoria=<?Php echo base64_encode($categoria[0]['kbcategoryid']);?>&articulo=<?Php echo base64_encode($dMoodle['kbarticleid']);?>"><?Php echo utf8_encode($dMoodle['subject']);?></a><br>
        							<?Php
        						}
                         	?>
                 <div class="team-member-link">
                   <a href="cfaq.php?categoria=<?Php echo base64_encode($categoria[0]['kbcategoryid']); ?>">Ver más</a> 
                 </div>
                </div>
              </div>
              <!-- Start single team member -->
              <!-- Start single team member -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                 <div class="team-member-img">
                   <img src="<?Php echo DEFAULT_LAYOUT;?>/images/blackboard.png" alt="team member img">
                 </div>
                 <div class="team-member-name">
                   <p><?Php echo $categoria[1]['title'];?></p>
                   
                   <span>(<?Php echo $categoria[1]['totalarticles'];?>)</span>
                 </div>
	                 <?Php 
                 	$obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
        					$sql='SELECT 
        						  	kbarticlelinkid, swkbarticles.kbarticleid, subject, swkbarticles.views
        						  FROM 
        						  	swkbarticles, swkbarticlelinks 
        						  WHERE 
        						  	swkbarticlelinks.kbarticleid = swkbarticles.kbarticleid AND
        						  	swkbarticlelinks.linktypeid=:idCat ORDER BY swkbarticles.views DESC LIMIT 5';
        					$obj->query($sql);				  
        				    $obj->bind(':idCat', $categoria[1]['kbcategoryid']);
                     	    $blackboard=$obj->resultset();
                            $obj->null;
                         	foreach($blackboard as $dBlackboard)
        						{
        							?>
        							<a href="question.php?categoria=<?Php echo base64_encode($categoria[1]['kbcategoryid']);?>&articulo=<?Php echo base64_encode($dBlackboard['kbarticleid']);?>"><?Php echo utf8_encode($dBlackboard['subject']);?></a><br>
        							
        							<?Php
        						}
                         	?>
                 <div class="team-member-link">
                   <a href="cfaq.php?categoria=<?Php echo base64_encode($categoria[1]['kbcategoryid']); ?>">Ver más</a> 
                 </div>
                </div>
              </div>
              <!-- Start single team member -->
              <!-- Start single team member -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                 <div class="team-member-img">
                   <img src="<?Php echo DEFAULT_LAYOUT;?>/images/alumnos.png" alt="team member img">
                 </div>
                 <div class="team-member-name">
                   <p><?Php echo $categoria[2]['title'];?></p>
                   
                   <span>(<?Php echo $categoria[2]['totalarticles'];?>)</span>
                 </div>
                 	<?Php 
                 	$obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
        					$sql='SELECT 
        						  	kbarticlelinkid, swkbarticles.kbarticleid, subject, swkbarticles.views
        						  FROM 
        						  	swkbarticles, swkbarticlelinks 
        						  WHERE 
        						  	swkbarticlelinks.kbarticleid = swkbarticles.kbarticleid AND
        						  	swkbarticlelinks.linktypeid=:idCat ORDER BY swkbarticles.views DESC LIMIT 5';
        					$obj->query($sql);				  
        				  $obj->bind(':idCat', $categoria[2]['kbcategoryid']);
                  $abuap=$obj->resultset();
                  $obj->null;
                         	foreach($abuap as $dAbuap)
        						{
        							?>
        							<a href="question.php?categoria=<?Php echo base64_encode($categoria[2]['kbcategoryid']);?>&articulo=<?Php echo base64_encode($dAbuap['kbarticleid']);?>"><?Php echo utf8_encode($dAbuap['subject']);?></a><br>
        							
        							<?Php
        						}
                         	?>
                 <div class="team-member-link">
                   <a href="cfaq.php?categoria=<?Php echo base64_encode($categoria[2]['kbcategoryid']); ?>">Ver más</a> 
                 </div>
                </div>
              </div>
              
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                 <div class="team-member-img">
                   <img src="<?Php echo DEFAULT_LAYOUT;?>/images/alumnos.png" alt="team member img">
                 </div>
                 <div class="team-member-name">
                   <p><?Php echo utf8_encode($categoria[3]['title']);?></p>
                   
                   <span>(<?Php echo $categoria[3]['totalarticles'];?>)</span>
                 </div>
                 	<?Php 
                 	$obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
        					$sql='SELECT 
        						  	kbarticlelinkid, swkbarticles.kbarticleid, subject, swkbarticles.views
        						  FROM 
        						  	swkbarticles, swkbarticlelinks 
        						  WHERE 
        						  	swkbarticlelinks.kbarticleid = swkbarticles.kbarticleid AND
        						  	swkbarticlelinks.linktypeid=:idCat ORDER BY swkbarticles.views DESC LIMIT 5';
        					$obj->query($sql);				  
        				  $obj->bind(':idCat', $categoria[3]['kbcategoryid']);
                  $abuap=$obj->resultset();
                  $obj->null;
                         	foreach($abuap as $dAbuap)
        						{
        							?>
        							<a href="question.php?categoria=<?Php echo base64_encode($categoria[2]['kbcategoryid']);?>&articulo=<?Php echo base64_encode($dAbuap['kbarticleid']);?>"><?Php echo utf8_encode($dAbuap['subject']);?></a><br>
        							
        							<?Php
        						}
                         	?>
                 <div class="team-member-link">
                   <a href="cfaq.php?categoria=<?Php echo base64_encode($categoria[3]['kbcategoryid']); ?>">Ver más</a> 
                 </div>
                </div>
              </div>
              <!-- Start single team member -->
              <!-- Start single team member 
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-team-member">
                 <div class="team-member-img">
                   <img src="<?Php echo DEFAULT_LAYOUT;?>/images/otras.png" alt="team member img">
                 </div>
                 <div class="team-member-name">
                   <p>Otras</p>
                   <span>(3)</span>
                 </div>
                 <a href="question.html">¿Cómo recupero mis datos de acceso del Correo Institucional?</a>
                 <div class="team-member-link">
                   <a href="#">Ver más</a>
                 </div>
                </div>
              </div>
              <!-- Start single team member -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Pricing table -->
<?Php
require_once 'footer/footer_v1.php';
?>
</body>
</html>