<!-- BEGIN MENU -->
<section id="menu-area">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php /*<a class="navbar-brand" href="http://dgie.buap.mx"><img  src="<?Php echo DEFAULT_LAYOUT;?>/images/logo.png"  alt="DGIE"></a> */ ?>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul id="top-menu" class="nav navbar-nav main-nav navbar-ccs">
                	<li><a href="index.php">Inicio</a></li>
					<li><a href="https://dcytic.buap.mx">DCyTIC</a></li>
					<li class="menu-item dropdown dropdown-submenu"><a href="#"  class="dropdown-toggle" data-toggle="dropdown">Recursos de Apoyo <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="supcat.php">Videotutoriales</a></li>
							<li><a href="faq.php">Preguntas Frecuentes</a></li>
							<li><a href="novedades-blackboard.php">Novedades</a></li>
						</ul>
					</li>
					<li><a href="catalogo_serviciosv1.php">Catálogo de Servicios</a></li>
					<li><a href="tienes_alguna_dudav2.php">Contáctanos</a></li>
						
                </ul>
            </div><!--/.nav-collapse -->
            <!--<a href="#" id="search-icon">
                <!--<i class="fa fa-search">
                </i>
            </a>-->
        </div>
    </nav>
</section>
<!-- END MENU -->
