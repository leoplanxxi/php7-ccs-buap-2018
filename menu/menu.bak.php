<!-- BEGIN MENU -->
<section id="menu-area">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php /*<a class="navbar-brand" href="http://dgie.buap.mx"><img  src="<?Php echo DEFAULT_LAYOUT;?>/images/logo.png"  alt="DGIE"></a> */ ?>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul id="top-menu" class="nav navbar-nav main-nav navbar-ccs">
                	<?Php
                	if($inicio==$option)
						{
						?>
						<li class="active" ><a href="index.php">Inicio</a></li>
						<?Php
						}
					else {
						?>
						<li ><a href="index.php">Inicio</a></li>
						<?Php
					}
                	?>
					<?Php
                    if($video==$option)
						{
							?>
							<li class="active"><a href="https://dcytic.buap.mx">DCyTIC</a></li>
							<?Php
						}
					else {
						?>
						<li><a href="https://dcytic.buap.mx/">DCyTIC</a></li>
						<?Php
					}
                    ?>
               		<?Php
                    if($video==$option)
						{
							?>
							<li class="active"><a href="category_videos.php">Video tutoriales</a></li>
							<?Php
						}
					else {
						?>
						<li><a href="category_videos.php">Video tutoriales</a></li>
						<?Php
					}
                    ?>

                    <!--<?Php
                	if($video==$option)
						{
						?>
		                    <li class="dropdown active">
		                        <a href="category_videos.php" class="dropdown-toggle" data-toggle="dropdown">Video tutoriales <span class="fa fa-angle-down"></span></a>
		                        <ul class="dropdown-menu" role="menu">
		                            <li><a href="moreview.php">Los más vistos</a></li>
		                            <?Php
		                             include_once 'funcion/funciones.php';
									 $AllCategory=AllCategory();
									 foreach ($AllCategory as $categoria) {
										 ?>
										 	<li><a href="category.php?category=<?Php echo base64_encode($categoria['idCat']); ?>"><?Php echo $categoria['nomCat'];?></a></li>
										 <?Php
									 }
		                            ?>
		                        </ul>
		                    </li>
		            <?Php
		            	}
					else {

						?>
							<li class="dropdown">
		                        <a href="category_videos.php" class="dropdown-toggle" data-toggle="dropdown">Video tutoriales <span class="fa fa-angle-down"></span></a>
		                        <ul class="dropdown-menu" role="menu">
		                            <li><a href="moreview.php">Los más vistos</a></li>
		                            <?Php
		                             include_once 'funcion/funciones.php';
									 $AllCategory=AllCategory();
									 foreach ($AllCategory as $categoria) {
										 ?>
										 	<li><a href="category.php?category=<?Php echo base64_encode($categoria['idCat']); ?>"><?Php echo $categoria['nomCat'];?></a></li>
										 <?Php
									 }
		                            ?>
		                        </ul>
		                    </li>
						<?Php
					}
		            ?>-->
		             
                    <?Php
                    if($faq==$option)
						{
							?>
							<li class="active"><a href="faq.php">Preguntas frecuentes</a></li>
							<?Php
						}
					else {
						?>
						<li><a href="faq.php">Preguntas frecuentes</a></li>
						<?Php
					}
                    ?>
					<!--MENÚ TIENES ALGUNA DUDA-->
                    <?Php
                    if($cont==$option)
						{
							?>
							<li class="active"><a href="tienes_alguna_duda.php">Contáctanos</a></li>
							<?Php
						}
					else {
						?>
						<li><a href="tienes_alguna_duda.php">Contáctanos</a></li>
						<?Php
					}
                    ?>
                    <!--MENÚ NOVEDADES BB-->
                    <?Php
                    if($novedades==$option)
						{
							?>
							<li class="active menunovedades"><a class="menunovedadesa" href="novedades-blackboard.php">Novedades Bb</a></li>
							<?Php
						}
					else {
						?>
						<li class="menunovedades"><a class="menunovedadesa" href="novedades-blackboard.php">Novedades Bb</a></li>
						<?Php
					}
                    ?>


                </ul>
            </div><!--/.nav-collapse -->
            <!--<a href="#" id="search-icon">
                <!--<i class="fa fa-search">
                </i>
            </a>-->
        </div>
    </nav>
</section>
<!-- END MENU -->
