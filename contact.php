<!DOCTYPE html>
<html lang="en">
<?Php
$cont=base64_encode('contacto');
$option=base64_encode('contacto');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!-- Start single page header -->
  <section id="single-page-header-contacto">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Contacto</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active">Contacto</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <section id="contact">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <div class="title-area">
              <h2 class="title">¿Tienes alguna pregunta?</h2>
              <span class="line"></span>            
            </div>
         </div>
         <div class="col-md-12">
           <div class="cotact-area">
             <div class="row">
               <div class="col-md-4">
                 <div class="contact-area-left">
                   <h4>Datos de contacto</h4>
                   <p>Dirección General de Innovación Educativa</p>
                   <address class="single-address">
                     <p>Edificio: 1DGIE Av. San Claudio y 22 Sur, 
						Ciudad Universitaria, Puebla, Pue.</p>
                     <p><a href="mailto:soporte.dgie@correo.buap.mx" style="color: #00B3E3; text-decoration:underline;">soporte.dgie@correo.buap.mx</a></p>
                     <p>+52(222) 2295500 ext. 5533 y 7902</p>
                     <p><strong> Horario de atención:</strong>
                     </p>
                     <p>
                     	Lunes a viernes de 08:00 a 18:00 horas.<br>
						Sábados de 08:00 a 12:00 horas.</p>
                   </address> 
                   <div class="footer-right contact-social">
                      <a href="<?Php echo FACEBOOK; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                      <a href="<?Php echo TWITTER; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                      <!--<a href="#"><i class="fa fa-linkedin"></i></a>
                      <a href="#"><i class="fa fa-google-plus"></i></a>-->
                      <a href="<?Php echo PINTEREST; ?>" target="_blank" ><i class="fa fa-pinterest"></i></a>
                    </div>                
                 </div>
               </div>
                  <link rel="stylesheet" href="js/jqwidgets/styles/jqx.base.css" type="text/css" />
				  <link rel="stylesheet" href="js/jqwidgets/styles/jqx.darkblue.css" type="text/css"/>
				  <script type="text/javascript" src="js/scripts/jquery-1.11.1.min.js"></script>
				  <script type="text/javascript" src="js/jqwidgets/jqxcore.js"></script>
				  <script type="text/javascript" src="js/jqwidgets/jqxexpander.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxvalidator.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxbuttons.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxcheckbox.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/globalization/globalize.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxcalendar.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxdatetimeinput.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxmaskedinput.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxlistbox.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxcombobox.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxscrollbar.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxinput.js"></script>
				  <script type="text/javascript" src="js/jqwidgets/jqxdata.js"></script>    
				  <script type="text/javascript" src="js/scripts/demos.js"></script> 
				  <script type="text/javascript" src="js/validar/contacto.js"></script>
				  
               <div class="col-md-8">
                 <div class="contact-area-right">
                   <form action="ecommet.php" class="comments-form contact-form" name="form" id="form" method="post">
                    <div class="form-group">                        
                      <input type="text" name="nombre" id="nombre" class="form-control" style="width: 525px" placeholder="Tu nombre">
                    </div>
                    <div class="form-group">                        
                      <input type="text" name="email" id="email"  class="form-control" style="width: 525px" placeholder="Correo electrónico">
                    </div>
                     <div class="form-group">                        
                      <input type="text" id="asunto" name="asunto" class="form-control" style="width: 525px" placeholder="Asunto">
                    </div>
                    <div class="form-group">                        
                      <textarea id="comentario" name="comentario"  placeholder="Comentario" style="width: 525px"  rows="3" class="form-control"></textarea>
                    </div>
                     <div class="form-group">
				        	<label> <div id="acceptTerms" name="acceptTerms"><strong><span class="text-danger"> Acepto que he leído el Aviso de Privacidad </span> </strong></div></label> 
				        </div>    
				      
			    <div class="form-group">
					<textarea name="aviso" style="text-align: justify" class="form-control input-mini" rows="3" cols="109"  readonly="readonly" >AVISO DE PRIVACIDAD: La Benem&eacute;rita Universidad Aut&oacute;noma de Puebla hace de su conocimiento que se recaban sus datos personales &uacute;nica y exclusivamente para el cumplimiento de los fines que legalmente tiene como organismo p&uacute;blico, y que se adoptan las medidas de seguridad administrativas, t&eacute;cnicas y f&iacute;sicas para garantizar el manejo y tratamiento adecuado de los mismos. Por tanto, ponemos a su disposici&oacute;n el Aviso de Privacidad Institucional en Extenso en la direcci&oacute;n electr&oacute;nica www.transparencia.buap.mx para conocer los derechos y obligaciones que se derivan del presente...
			    	</textarea>
				</div>
                    <input type="button" id="sendButton" class="comment-btn" value="Enviar comentario"/>
                  </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
  </section>
  <!-- End contact section  -->
  <!-- Start google map -->
  <section id="google-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3772.3956452702696!2d-98.20053468466025!3d19.002279859222526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfbf608de241cf%3A0x2a975de83ee53662!2sDirecci%C3%B3n+General+de+Innovaci%C3%B3n+Educativa+de+la+BUAP!5e0!3m2!1ses!2smx!4v1453222810508" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </section>
  <!-- End google map -->
<?Php
require_once 'footer/footer_v1.php';
?>
</body>
</html>