<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<?Php
$faq=base64_encode('faq');
$option=base64_encode('faq');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
//echo base64_decode($_GET[categoria]);
switch (base64_decode($_GET[categoria])) {
	case 9:
		$header='single-page-header-faq-moodle';
		$categoria='Moodle';
		break;
	case 10:
		$header='single-page-header-faq-blackboard';
		$categoria='Blackboard';
		break;
	case 11:
		$header='single-page-header-faq-alumnosb';
		$categoria='Alumnos - BUAP';
		break;
	case 13:
    $header='single-page-header-faq-alumnosb';
    $categoria='Proceso de Admisión: Modalidades Semiescolarizada y a Distancia';
    break;
}
?>
<?Php 
require_once 'funcion/funciones.php';
$article=Buscaquestion($_GET['articulo']);
date_default_timezone_set ('America/Mexico_City');
?>
<!-- Start single page header -->
  <section id="<?Php echo $header;?>">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Preguntas frecuentes / <?Php echo $categoria;?></h2>
              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>-->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li><a href="faq.php">Preguntas frecuentes</a></li>
                <li><a href="#"><?Php echo $categoria;?></a></li>
                <li class="active"> <?Php echo utf8_encode($article[0]['subject']);?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <!-- Start blog archive -->
  <section id="blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="blog-archive-area">
            <div class="row">
              <div class="col-md-8">
                <div class="blog-archive-left">
                  <!-- Start blog news single -->
                  <article class="blog-news-single">                
                    <div class="blog-news-title">
                     <h2><?Php echo utf8_encode($article[0]['subject']);?></h2>
                      <p>Publicado por <a class="blog-author" href="#"><?Php echo utf8_encode($article[0]['fullname']);?></a> <span class="blog-date"><?Php echo date('Y-m-d h:i:s a', $article[0]['dateline']);?></span></p>
                    </div>
                    <div class="blog-news-details blog-single-details">
                      <?Php echo $article[0]['contents'];?>
                      <div class="blog-single-bottom">
                        <div class="row">
                          <div class="col-md-8">
                            <div class="blog-single-tag">
                              <!--<span class="fa fa-tags"></span>
                              <a href="#">Inicio</a>,
			                  Video tutoriales,
			                  <a href="#">Categor&iacute;a: Centro de calificaciones</a>,
			                  Columna total y ponderada -->
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="blog-single-social">
                              <a href="<?Php echo FACEBOOK; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                              <a href="<?Php echo TWITTER; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                              <!--<a href="#"><i class="fa fa-linkedin"></i></a>
                              <a href="#"><i class="fa fa-google-plus"></i></a>-->
                              <a href="<?Php echo PINTEREST; ?>" target="_blank" ><i class="fa fa-pinterest"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </article>
                  <link rel="stylesheet" href="js/jqwidgets/styles/jqx.base.css" type="text/css" />
				  <link rel="stylesheet" href="js/jqwidgets/styles/jqx.darkblue.css" type="text/css"/>
				  <script type="text/javascript" src="js/scripts/jquery-1.11.1.min.js"></script>
				  <script type="text/javascript" src="js/jqwidgets/jqxcore.js"></script>
				  <script type="text/javascript" src="js/jqwidgets/jqxexpander.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxvalidator.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxbuttons.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxcheckbox.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/globalization/globalize.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxcalendar.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxdatetimeinput.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxmaskedinput.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxlistbox.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxcombobox.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxscrollbar.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxinput.js"></script>
				  <script type="text/javascript" src="js/jqwidgets/jqxdata.js"></script>    
				  <script type="text/javascript" src="js/scripts/demos.js"></script> 
			      <script type="text/javascript" src="js/validar/qcomments.js"></script> 
			      
                  <!-- Start Comment box -->
                  <div class="comments-box-area">
                    <h2>Deja tu comentario</h2>
                    <p>Tu direcci&oacute;n de correo electr&oacute;nico no ser&aacute; publicada.</p>
                    <form action="insertqcomment.php" class="comments-form" method="post" name="form" id="form">
                    	<input type="hidden" name="categoria" id="categoria" value="<?Php echo ($_GET['categoria']);?>"/>
                    	<input type="hidden" name="articulo" id="articulo" value="<?Php echo ($_GET['articulo']);?>"/>
                       <div class="form-group">                        
                        <input type="text" name="nombre" id="nombre" class="form-control" style="width: 525px" placeholder="Tu nombre" >
                      </div>
                      <div class="form-group">                        
                        <input type="text" name="email" id="email" class="form-control" style="width: 525px" placeholder="Correo electr&oacute;nico">
                      </div>
                       <div class="form-group">                        
                        <textarea placeholder="Comentario" id="comentario" name="comentario" rows="3" class="form-control"></textarea>
                      </div>
                      <div class="form-group">
				        	<label> <div id="acceptTerms" name="acceptTerms"><strong><span class="text-danger"> Acepto que he leído el Aviso de Privacidad </span> </strong></div></label> 
				        </div>    
				      
			    <div class="form-group">
					<textarea name="aviso" style="text-align: justify" class="form-control input-mini" rows="3" cols="109"  readonly="readonly" >AVISO DE PRIVACIDAD: La Benem&eacute;rita Universidad Aut&oacute;noma de Puebla hace de su conocimiento que se recaban sus datos personales &uacute;nica y exclusivamente para el cumplimiento de los fines que legalmente tiene como organismo p&uacute;blico, y que se adoptan las medidas de seguridad administrativas, t&eacute;cnicas y f&iacute;sicas para garantizar el manejo y tratamiento adecuado de los mismos. Por tanto, ponemos a su disposici&oacute;n el Aviso de Privacidad Institucional en Extenso en la direcci&oacute;n electr&oacute;nica www.transparencia.buap.mx para conocer los derechos y obligaciones que se derivan del presente...
			    	</textarea>
				</div>
                      <input type="button" id="sendButton" class="comment-btn" value="Enviar comentario"/>
                      
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <aside class="blog-side-bar">
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <h4 class="widget-title">Comentarios</h4>
                    <?Php 
                    $comments=BComentsQuestion($_GET['articulo']);
					foreach ($comments as $dcomments) {
							if($dcomments['creatorid']<>0)
								{
									?>
									<p style="color: #265465"><i><?Php echo utf8_encode($dcomments['contents'])?></i></p>
									<p><span style="color: #265465"><i>Publicado por</i></span> <a class="blog-author" href="#"><strong><i><?Php echo utf8_encode($dcomments['fullname']);?></i></strong></a><span class="blog-date" style="color: #265465"><i>  <?Php echo date('Y-m-d h:i:s a', $dcomments['dateline']);?></i> </span></p>
									<hr>
									<?Php
								}
							else {
								?>
								<p><?Php echo utf8_encode($dcomments['contents'])?></p>
								<p>Publicado por <strong><?Php echo utf8_encode($dcomments['fullname']);?></strong><span class="blog-date" > <?Php echo date('Y-m-d h:i:s a', $dcomments['dateline']);?></span></p>
								<hr>
								<?Php
							}
						?>
						<?Php
					}
                    ?>                  
                  </div> 
                </aside>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </section>
  <!-- End blog archive -->
<?Php
require_once 'footer/footer_v1.php';
?>
</body>
</html