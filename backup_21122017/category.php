<!DOCTYPE html>
<html lang="en">
<?Php
$video=base64_encode('video');
$option=base64_encode('video');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
if(isset($_GET['category']) && !empty($_GET['category']))
	{
		$searchCategory=searchCategory($_GET['category']);
		foreach($searchCategory as $category)
			{
				$nomCategoria=$category['nomCat'];
				$imgCategoria=$category['imgCat'];
			}
	}
else {
	echo "<script language='JavaScript'>";
	echo "location = 'index.php'";
	echo "</script>";
}
?>
<!-- Start single page header -->
  <section id="single-page-header-faq-videot">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Video tutoriales / Categoría: <?Php echo $nomCategoria;?></h2>
              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>-->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li>Video tutoriales</li>
                <li class="active">Categoría: <?Php echo $nomCategoria;?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <!-- Start Pricing table -->
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/<?Php echo $imgCategoria;?>" alt="<?Php echo $nomCategoria;?>"> <?Php echo $nomCategoria;?></h2>
            <span class="line"></span>
            <!--<p>Haz clic en el video correspondiente</p>-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content">
            <div class="row">
              <table id="categoria" class="display" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th> </th>
			                <th>Video</th>
			                <th>Descripción</th>
			                <th>duración</th>  
			            </tr>
			        </thead>
			        <tfoot>
			            <tr>
			                <th> </th>
			                <th>Video</th>
			                <th>Descripción</th>
			                <th>duración</th>		             
			            </tr>
			        </tfoot>
			        <tbody>
			            <?Php 
			            $searchList=searchList($_GET['category']);
						foreach($searchList as $videos)
							{
								?>
								<tr>
					                <td><?Php echo $num;?></td>
					                <td>
					                	<a href="view.php?category=<?Php echo $_GET['category']; ?>&video=<?Php echo $videos['idVideo'];?>">
					                		<img src="https://img.youtube.com/vi/<?Php echo $videos['idVideo']; ?>/default.jpg" alt="team member img">
					                	</a>
					                </td>
					                <td>
					                	<?Php echo $videos['titulo']; ?><br>
										de <?Php echo $videos['autor']; ?>
									</td>
					                <td><?Php echo $videos['duracion']; ?></td>      
					            </tr>
								<?Php
							}
			            ?>			            
			        </tbody>
			    </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Pricing table --> 
  
<?Php
require_once 'footer/footer_v1.php';
?>
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.dataTables.min.js"></script>  
<script type="text/javascript">
      $(document).ready(function() {
		    $('#categoria').DataTable( {
		        "paging":   true,
		        "ordering": false,
		        "info":     false
		    } );
		} );
    </script>  
</body>
</html>