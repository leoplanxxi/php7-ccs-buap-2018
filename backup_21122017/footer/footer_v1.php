<!-- Start footer -->
<footer id="footer">
<!-- Container: centra el contenido y agrega un margen -->
    <div class="container">
    	<!-- Row: agregar filas -->
        <div class="row">
        	<!-- Row: agregar filas -->
            <!-- col-xs: extra small, celulares; col-sm: celulares y tabletas, col-md: tabletas grandes, formato horizontal; col-lg: computadoras de escritorio-->
            <!--<div class="col-xs-12 col-md-12">-->
                <!--<div class="footer-left">-->
                    <!--<div class="row">-->
                    	
                        
                        <!--ENLACES E INFORMACION-->
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 enlaces" style="vertical-align: middle; text-align:center; margin-bottom:15px">
                        <div style="padding-bottom:5px"><span style="font-size: 12px; color: #00B8E0; text-align:center; margin-bottom:5px">Enlaces BUAP</span></div>
                        
                            <a href="http://www.amereiaf.mx/" target="_blank"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/01-amereiaf.png" /></a>
                            <a href="http://crcs.anuies.mx/" target="_blank"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/02-anuies.png" /></a>
                            <a href="http://cmas.siu.buap.mx/portal_pprd/wb/contraloria_social/contraloria_social" target="_blank"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/03-contraloria-social.png" /></a>
                            <a href="http://www.transparencia.buap.mx/" target="_blank"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/04-transparencia.png" /></a>
                            <br>
                            <a href="http://www.farmaciasflemingbuap.com/" target="_blank"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/05-farmacias-fleming.png" /></a>
                            <a href="http://clublobosbuap.com/" target="_blank"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/06-lobos-buap.png" /></a>
                            <a href="http://www.sorteo.buap.mx/" target="_blank"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/07-sorteos-buap.png" /></a>
                            <a href="http://www.ccu.buap.mx/" target="_blank"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/08-ccu.png" /></a>
                        </div>
                        
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 informacion" style="margin-top:-5px; margin-bottom:15px" >
                        	
                              <a href="http://cmas.siu.buap.mx/portal_pprd/wb/rectoria/avisos" target="_blank" style="font-size: 11px;color: #00B8E0">Aviso de Privacidad</a><br>
                              <a href="http://cmas.siu.buap.mx/portal_pprd/wb/BBUAP/directorio_telefonico_2013" target="_blank" style="font-size: 11px;color: #00B8E0">Directorio Telefónico</a><br>
                              <a href="#" style="font-size: 11px;color: #00B8E0">Mapa del Sitio</a>
                              
                        </div>
                        
                        
                        <!--DIRECCION Y ESCUDO-->
                         <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 direccion" style="margin-top: -5px; margin-bottom:40px">
            				<span style="font-size: 12px; color: #00B8E0">Dirección General de Innovación Educativa <br>Edificio: 1DGIE Av. San Claudio y 22 Sur, <br>Ciudad Universitaria, Puebla, Pue.<br> Teléfono +52(222) 2295500 ext. 5533 y 7902</span>
                        </div>
                        
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 logo-buap60" style="margin-top:-15px;">
                            <img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/escudo-bottom.png" style="align-content:center;"/>
                        </div>
                        
                        
                      <!--  <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="background-color:#543838; height: 50px"></div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="background-color:#543838; height: 50px"></div>-->
                        <!--<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="background-color:#B53436; height: 50px"></div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="background-color:#B53436; height: 50px"></div>-->
                        
                        
                    <!--</div>-->
                <!--</div>-->
            <!--</div>-->
            
        </div>
    </div>
</footer>
<!-- End footer -->
 <!-- jQuery library -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap -->
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/bootstrap.js"></script>
<!-- Slick Slider -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/slick.js"></script>
<!-- mixit slider -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.fancybox.pack.js"></script>
<!-- counter -->
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/waypoints.js"></script>
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.counterup.js"></script>
<!-- Wow animation -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/wow.js"></script>
<!-- progress bar   -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/bootstrap-progressbar.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/custom.js"></script>
  