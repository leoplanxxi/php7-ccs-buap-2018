<!DOCTYPE html>
<html lang="en">
<?Php
$video=base64_encode('video');
$option=base64_encode('video');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
/*require_once 'funcion/funciones.php';*/
?>
<!-- Start single page header -->
  <section id="single-page-header-faq-videot">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Video tutoriales / Categoría: <?Php echo $nomCategoria;?></h2>
              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>-->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li>Video tutoriales</li>
                <li class="active">Categorías</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <!-- Start Pricing table -->
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Categoría: Los más vistos</h2>
            <span class="line"></span>
            <!--<p>Haz clic en el video correspondiente</p>-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content">
            <div class="row">
             <table id="moreview" class="display" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th> </th>
			                <th>Categoría</th>
			                <th>Descripción</th>
			                <th>Total de videos</th>  
			            </tr>
			        </thead>
			        <tfoot>
			            <tr>
			                <th></th>
			                <th>Categoría</th>
			                <th>Descripción</th>
			                <th>Total de videos</th>		             
			            </tr>
			        </tfoot>
			        <tbody>
                    	<tr>
                        	<td></td>
                        	<td>
					             <a href="moreview.php"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/00-mas-vistos.png" alt="los videos más vistos"></a>
					        </td>
                            <td>
					              <a href="moreview.php">Los más vistos</a>
							</td>
                            <td>
                            	10
                            </td>
                        </tr>
			            <?Php 
			            $todas_las_categorias=AllCategory();
						
						foreach($todas_las_categorias as $cat)
							{
								?>
								
                                <tr>
					                <td></td>
					                <td>
					                	<a href="category.php?category=<?Php echo base64_encode($cat['idCat']); ?>"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/<?Php echo $cat['imgCat'];?>" alt="<?Php echo $cat['nomCat'];?>"></a>
					                </td>
					                <td>
					                	<a href="category.php?category=<?Php echo base64_encode($cat['idCat']); ?>"><?Php echo $cat['nomCat'];?></a>
									</td>
					                <td>
                                    	<?Php 
											$idCategoria=base64_encode($cat['idCat']);
											$totalCategoria=totalCategory($idCategoria);
											/*print_r($totalCategoria);*/
											/*echo $idCategoria;*/
											echo $totalCategoria[0]['total'];
										?>
                                    </td>      
					            </tr>
								<?Php
							}
			            ?>			            
			        </tbody>
			    </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Pricing table --> 
  
<?Php
require_once 'footer/footer_v1.php';
?>
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.dataTables.min.js"></script>  
<script type="text/javascript">
      $(document).ready(function() {
		    $('#moreview').DataTable( {
		        "paging":   true,
		        "ordering": false,
		        "info":     false
		    } );
		} );
    </script>  
</body>
</html>