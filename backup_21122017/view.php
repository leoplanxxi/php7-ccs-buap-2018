<!DOCTYPE html>
<html lang="en">
<?Php
$video=base64_encode('video');
$option=base64_encode('video');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
if(isset($_GET['category']) && !empty($_GET['category']))
	{
		if(isset($_GET['video']) && !empty($_GET['video']))
			{
				$searchCategory=searchCategory($_GET['category']);
				foreach($searchCategory as $category)
					{
						$nomCategoria=$category['nomCat'];
					}
				$searchVideo=searchVideo($_GET['video']);
				
				foreach($searchVideo as $video)
					{
						$vtitulo=$video['titulo'];
						$vautor=$video['autor'];
						$vdescripcion=$video['descripcion'];
						$vfecha=$video['fpublicacion'];
						$mam=$video['mam'];
						if(empty($mam))
							{
								$mam=0;
							}
						$ma=$video['ma'];
						if(empty($ma))
							{
								$ma=0;
							}
						$sc=$video['sc'];
						if(empty($sc))
							{
								$sc=0;
							}
						$map=$video['map'];
						if(empty($map))
							{
								$map=0;
							}	
						$nma=$video['nma'];
						if(empty($nma))
						{
							$nma=0;
						}
						list($anno,$mon,$day)=explode("-", $vfecha);
						$meses = array("enero","Febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
						$mes=$meses[date($mon)-1];
						$vfecha=$day .' de '. $mes . ' de '.$anno;
						$vvisitas=$video['visitas'];
						if(empty($vvisitas))
							{
								$vvisitas=$vvisitas+1;
								$vistasr=array('visitas'=>$vvisitas,"idVideo"=>$_GET['video']);
								$setVistas=setVistas($vistasr);
							}
						else {
							$vvisitas=$vvisitas+1;
							$vistasr=array('visitas'=>$vvisitas,"idVideo"=>$_GET['video']);
							$setVistas=setVistas($vistasr);
						}
						
					}
			}
	}
else {
	echo "<script language='JavaScript'>";
	echo "location = 'index.php'";
	echo "</script>";
}
?>
<link rel="stylesheet" href="style_icon.css">

<!-- Start single page header -->
  <section id="single-page-header-faq-videot">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Video tutoriales / Categoría: <?Php echo $nomCategoria;?></h2>
              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>-->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li>Video tutoriales</li>
                <li class="active"><a href="category.php?category=<?Php echo $_GET['category'];?>">Categoría: <?Php echo $nomCategoria;?></a> </li>
                <li class="active"><?Php echo $vtitulo;?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <!-- Start blog archive -->
  <section id="blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="blog-archive-area">
            <div class="row">
              <div class="col-md-8">
                <div class="blog-archive-left">
                  <!-- Start blog news single -->
                  <article class="blog-news-single">
                  	<br>
                    <div class="blog-news-img">
                    	<center>
                      		<iframe width="640" height="360" src="https://www.youtube.com/embed/<?Php echo $_GET['video']; ?>?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>
                      	</center>
                      	<span style="float: right"><?Php echo $vvisitas;?> visualizaciones</span> 
                    </div>
                    <div class="blog-news-title">
                      <h2>Video: <?Php echo $vtitulo;?></h2>
                      <p>Por <a class="blog-author" href="#"><?Php echo $vautor;?></a> <span class="blog-date">| <?Php echo $vfecha;?></span></p>
                    </div>
                    <div class="blog-news-details blog-single-details">
                      <?Php echo $vdescripcion;?><br>
                      <div class="blog-single-bottom">
                        <div class="row">
                          <div class="col-md-8">
                            <div class="blog-single-tag">
                            	<link rel="stylesheet" href="js/jqwidgets/styles/jqx.base.css" type="text/css" />
								  <link rel="stylesheet" href="js/jqwidgets/styles/jqx.darkblue.css" type="text/css"/>
								  <script type="text/javascript" src="js/scripts/jquery-1.11.1.min.js"></script>
								  <script type="text/javascript" src="js/jqwidgets/jqxcore.js"></script>
								  <script type="text/javascript" src="js/jqwidgets/jqxexpander.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxvalidator.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxbuttons.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxcheckbox.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/globalization/globalize.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxcalendar.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxdatetimeinput.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxmaskedinput.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxlistbox.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxcombobox.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxscrollbar.js"></script> 
								  <script type="text/javascript" src="js/jqwidgets/jqxinput.js"></script>
								  <script type="text/javascript" src="js/jqwidgets/jqxdata.js"></script>
								  <script type="text/javascript" src="js/jqwidgets/jqxtooltip.js"></script>    
								  <script type="text/javascript" src="js/scripts/demos.js"></script> 
							      <script type="text/javascript" src="mam.js" ></script>
							      <script type="text/javascript" src="ma.js" ></script>
							      <script type="text/javascript" src="sc.js" ></script>
							      <script type="text/javascript" src="map.js" ></script>
							      <script type="text/javascript" src="nma.js" ></script>
                    <script type="text/javascript" src="js/validar/qcommentsv.js"></script> 
							      <script type="text/javascript">
						            $(document).ready(function () {					              
						                $("#mam").jqxButton({ template: "link" });
						                $("#ma").jqxButton({ template: "link" });
						                $("#sc").jqxButton({ template: "link" });
						                $("#map").jqxButton({ template: "link" });
						                $("#nma").jqxButton({ template: "link" });
						                
						                $("#mam").jqxTooltip({ content: 'Me ayudo mucho', position: 'bottom', name: 'movieTooltip'});
						                $("#ma").jqxTooltip({ content: 'Me ayudo', position: 'bottom', name: 'movieTooltip'});
						                $("#sc").jqxTooltip({ content: 'Sin comentario', position: 'bottom', name: 'movieTooltip'});
						                $("#map").jqxTooltip({ content: 'Me ayudo poco', position: 'bottom', name: 'movieTooltip'});
						                $("#nma").jqxTooltip({ content: 'No me ayudo', position: 'bottom', name: 'movieTooltip'});
						            });
						        </script>
						        
                            	<table>
                            		<tr>
                            			<td style="text-align: center">
                            			 <form id="fmam" method="post" action="">
                            			 	<input type="hidden" name="idVideo" id="idVideo" value="<?Php echo $_GET['video']; ?>" />
                            			 	<button id="mam" type="submit">
                            					<i class="icon-laughing-face" style="font-size: 35px;font-weight:400"></i>
                            				</button>
                            				<div id="tmam"><span style="font-weight: 600"><?Php echo $mam;?></span></div>
                            			 </form>
                            				
                            			</td>
                            			<td style="text-align: center">
                            			<form id="fma" method="post" action="">
                            				<input type="hidden" name="idVideo" id="idVideo" value="<?Php echo $_GET['video']; ?>" />
                            				<button id="ma" type="submit">
                            					<i class="icon-winking-face" style="font-size: 35px;font-weight:400"></i>
                            				</button>
                            				<div id="tma"><span style="font-weight: 600"><?Php echo $ma;?></span></div>
                            			</form>                           				
                            			</td>
                            			<td style="text-align: center">
                            			<form id="fsc" method="post" action="">
                            				<input type="hidden" name="idVideo" id="idVideo" value="<?Php echo $_GET['video']; ?>" />
                            				<button id="sc" type="submit">
                            					<i class="icon-neutral-face3" style="font-size: 35px;font-weight:400"></i>
                            				</button>
                            				<div id="tsc"><span style="font-weight: 600"><?Php echo $sc;?></span></div>	
                            			</form>                               				
                            			</td>
                            			<td style="text-align: center">
                            			<form id="fmap" method="post" action="">
                            				<input type="hidden" name="idVideo" id="idVideo" value="<?Php echo $_GET['video']; ?>" />
                            				<button id="map">
                            					<i class="icon-sad-face" style="font-size: 35px;font-weight:400"></i>
                            				</button>
                            				<div id="tmap"><span style="font-weight: 600"><?Php echo $map;?></span></div>
                            			</form>                            				
                            			</td>
                            			<td style="text-align: center">
                            			<form id="fnma" method="post" action="">
                            				<input type="hidden" name="idVideo" id="idVideo" value="<?Php echo $_GET['video']; ?>" />
                            				<button id="nma">
                            					<i class="icon-sad-face3" style="font-size: 35px;font-weight:400"></i>
                            				</button>
                            				<div id="tnma"><span style="font-weight: 600"><?Php echo $nma;?></span></div>
                            			</form>                        				
                            			</td>
                            		</tr>
                            	</table>    
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="blog-single-social">
                              <a href="<?Php echo FACEBOOK; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                              <a href="<?Php echo TWITTER; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                              <!--<a href="#"><i class="fa fa-linkedin"></i></a>
                              <a href="#"><i class="fa fa-google-plus"></i></a>-->
                              <a href="<?Php echo PINTEREST; ?>" target="_blank" ><i class="fa fa-pinterest"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </article>
                  <!-- Start Comment box -->
                  <div class="comments-box-area">
                    <h2>Deja tu comentario</h2>
                    <p>Tu dirección de correo electrónico no será publicada.</p>
                     <form action="insertqcommentv.php" class="comments-form" method="post" name="form" id="form">
                     	<input type="hidden" name="categoria" id="categoria" value="<?Php echo ($_GET['category']);?>"/>
                    	<input type="hidden" name="video" id="video" value="<?Php echo ($_GET['video']);?>"/>
                        <div class="form-group">                        
                        	<input type="text" name="nombre" id="nombre" class="form-control" style="width: 525px" placeholder="Tu nombre" >
                      	</div>
                      	<div class="form-group">                        
                        	<input type="text" name="email" id="email" class="form-control" style="width: 525px" placeholder="Correo electr&oacute;nico">
                      	</div>
                       	<div class="form-group">                        
                        	<textarea placeholder="Comentario" id="comentario" name="comentario" rows="3" class="form-control"></textarea>
                      	</div>
                      	
						<div class="form-group">
				        	<label> <div id="acceptTerms" name="acceptTerms"><strong><span class="text-danger"> Acepto que he leído el Aviso de Privacidad </span> </strong></div></label> 
				        </div>    
				      
			    <div class="form-group">
					<textarea name="aviso" style="text-align: justify" class="form-control input-mini" rows="3" cols="109"  readonly="readonly" >AVISO DE PRIVACIDAD: La Benem&eacute;rita Universidad Aut&oacute;noma de Puebla hace de su conocimiento que se recaban sus datos personales &uacute;nica y exclusivamente para el cumplimiento de los fines que legalmente tiene como organismo p&uacute;blico, y que se adoptan las medidas de seguridad administrativas, t&eacute;cnicas y f&iacute;sicas para garantizar el manejo y tratamiento adecuado de los mismos. Por tanto, ponemos a su disposici&oacute;n el Aviso de Privacidad Institucional en Extenso en la direcci&oacute;n electr&oacute;nica www.transparencia.buap.mx para conocer los derechos y obligaciones que se derivan del presente...
			    	</textarea>
				</div>
                      	<input type="button" id="sendButton" class="comment-btn" value="Enviar comentario"/>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <aside class="blog-side-bar">
                  <!-- Start sidebar widget -->
                  <div class="sidebar-widget">
                    <h4 class="widget-title">Comentarios</h4>
                    <?Php 
                    $BComentsVideos=BComentsVideos($_GET['video']);
						foreach($BComentsVideos as $dcomments)
							{		
								if($dcomments['creatorid']<>0)
									{
										?>
										<p style="color: #265465"><i><?Php echo ($dcomments['contents'])?></i></p>
										<p><span style="color: #265465"><i>Publicado por</i></span> <a class="blog-author" href="#"><strong><i><?Php echo utf8_encode($dcomments['fullname']);?></i></strong></a><span class="blog-date" style="color: #265465"><i>  <?Php echo date('Y-m-d h:i:s a', $dcomments['dateline']);?></i> </span></p>
										<hr>
										<?Php
									}
								else {
									?>
									<p><?Php echo ($dcomments['contents'])?></p>
									<p>Publicado por <strong><?Php echo ($dcomments['fullname']);?></strong><span class="blog-date" > <?Php echo date('Y-m-d h:i:s a', $dcomments['dateline']);?></span></p>
									<hr>
									<?Php
								}					
							}
                    ?>
                  </div> 
                </aside>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </section>
  <!-- End blog archive -->
<?Php
require_once 'footer/footer.php';
?>
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/demo.js"></script>
</body>
</html>