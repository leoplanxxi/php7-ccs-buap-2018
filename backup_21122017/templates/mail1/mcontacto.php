<?Php 
require_once('aplication/libs/correo/class.phpmailer.php');
$mail = new PHPMailer();	
$message = '<html>';
 //$message .='<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
$message .='<style type="text/css">
				.frase {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 8px;
				color: #989898;
				text-align: left;
				text-decoration: none;
			}
			.nombre {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
				font-weight: bold;
				color: #003B5C;
				text-align: left;
				margin: 0px;
			}
			.apellido {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 11px;
				font-weight: bold;
				color: #003B5C;
				text-align: left;
				margin: 0px;
			}
			.cargo {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 10px;
				font-style: oblique;
				color: #003B5C;
				line-height: 10px;
				text-align: left;
				vertical-align: middle;
				padding: 0px;
				margin-top: 5px;
				margin-right: 0px;
				margin-bottom: 0px;
				margin-left: 0px;
			}
			.bold {
				font-family: Arial, Helvetica, sans-serif;
				font-weight: bold;
				color: #003B5C;
				text-align: left;
				font-size: 10.5px;
			}
			.texto {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 10px;
				line-height: 12px;
				color: #003B5C;
				text-align: left;
				margin: 0px;
			}
			.informacion {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 11px;
				color: #003B5C;
				text-align: left;
				vertical-align: middle;
				margin: 0px;
				padding: 0px;
			}
			a {
				text-decoration: none;
			}
			.link {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 11px;
				color: #003B5C;
				text-align: left;
				text-decoration: underline;
			}
			</style>';
$message .='<body>';
$message .='<strong>Estimad@: </strong><span class="nombre">'.$nombre.'.<br><br></span>';
$message .='En breve personal del área de soporte le dará seguimiento a tu pregunta <p>&nbsp;</p>';
$message .='<span class="nombre">asunto:'.$asunto.'</span><br>';
$message .='<span class="nombre">'.$comentario.'</span>';
$message .='<p>&nbsp;</p>';
 //startfirma
$message .='
            <table width="600" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td height="35" colspan="6" class="frase" style="border-top:1px solid #CCCCCC"><hr /></td>
			  </tr>
			  <tr>
			    <td width="87" rowspan="4" style="padding: 0; vertical-align:top"><a href="http://www.buap.mx/" target="_blank"><img src="http://dgie.buap.mx/firma/imagenes/buap.png" width="83" height="90" alt="BUAP" style="margin: 0"/></a></td>
			    <td colspan="2" rowspan="3" valign="middle" style="border-right:1px solid #CCCCCC; padding: 0 10px 0 0" ><span class="nombre">Soporte técnico</span><br />
			    <p class="cargo">Administración de Sistemas</p></td>
			    <td width="29" rowspan="3" ><img src="http://dgie.buap.mx/firma/imagenes/iconos.gif" width="19" height="67" alt="Contactos" style="margin-left: 5px; margin-right: 5px" /></td>
			    <td height="22" colspan="2" style="padding: 0"><a href="mailto:soporte.dgie@correo.buap.mx" class="texto">soporte.dgie@correo.buap.mx</a><span class="frase"> | </span></td>
			  </tr>
			  <tr>
			    <td height="22" colspan="2" valign="middle" class="informacion" style="padding: 0"><a href="tel:2222295500" class="texto">(222) 2 29 55 00</a><span class="frase"> | </span> <span class="informacion">Ext:  7902</span></td>
			  </tr>
			  <tr>
			    <td width="145" height="22" valign="middle" style="padding: 0;"><span class="link"><a href="http://www.dgie.buap.mx/soporte/" target="_blank" class="texto">http://dgie.buap.mx/soporte/</a></span><span class="frase"> |</span></span></td>
			    <td width="201" valign="middle" class="texto" style="padding: 0;"><a href="https://www.facebook.com/BUAPaDistancia" target="_blank"><img src="http://dgie.buap.mx/firma/imagenes/facebook.gif" alt="Facebook - BUAPaDistancia" width="25" height="17" /></a><a href="https://twitter.com/BUAPaDistancia" target="_blank"><img src="http://dgie.buap.mx/firma/imagenes/twitter.gif" alt="Twitter - BUAPaDistancia" width="25" height="17" /></a></td>
			  </tr>
			  <tr style="margin-top: 5" valign="top">
			    <td width="30" style="padding: 5px 0 0 0"><a href="http://www.dgie.buap.mx/" target="_blank"><span class="bold">dgie</span><span class="frase"> |</span></a></td>
			    <td width="108" class="texto" style="padding: 5px 0 0 0"><a href="http://www.dgie.buap.mx/" target="_blank" class="texto">Dirección General de Innovación Educativa</a></td>
			    <td colspan="3" class="texto" style="padding: 5px 0 0 10px">Lunes a Viernes de 9:00 - 16:00 hrs.<span class="frase"> | </span>Edif. 1DGIE, Av. San Claudio y 22 Sur, s/n, Cuidad Universitaria. Puebla, Pue. C.P. 72570 </td>
			  </tr>
			  <tr>
			    <td height="13px" colspan="6" class="frase" style="border-bottom: 1px solid #CCCCCC"><hr /><p><strong>ADVERTENCIA DE PRIVACIDAD. </strong>La información contenida en este correo electrónico y archivos adjuntos, es privada, confidencial, y para uso exclusivo del destinatario, por lo que la utilización indebida de la misma será responsabilidad directa de quien lo realice. Si Usted no es el destinatario y recibió éste por error, evite su utilización, reproducción, o difusión, debiendo eliminarlo de su computadora o cualquier dispositivo electrónico y comunicarlo inmediatamente a su emisor, quien goza de las salvaguardas previstas por los artículos 1, 2 fracción VI, 25 y 27 apartado A, fracción IV, de la Ley de Protección de Datos Personales en Posesión de los Sujetos Obligados del Estado de Puebla, y de las políticas y lineamientos en materia de datos personales del Estado de Puebla, y sus correlativos de la Federación, incluyendo los aplicables a entidades privadas.
			    <hr />
			    <strong>STATEMENT OF CONFIDENTIALITY.</strong> The information contained in this e-mail and its attached files is private and confidential, and for sole use by the recipient. The improper use of such information will be the direct responsibility of the person using it. If you are not the intended recipient and you received this e-mail, avoid its use, reproduction or disclosure. You should delete it from your computer or any electronic device and immediately inform the sender, who has the safeguards of Articles 1, 2 section VI, 25 and 27, paragraph A, section IV of the Personal Data Protection in Possession of Bound Subjects Act of the State of Puebla, and of the policies and guidelines in personal information of the State of Puebla, and its related articles in federal law, including those applicable to private entities. <p></p>
			    </td>
			  </tr>
			</table>';
 //endfirm
$message .= '</body>';
$message .= '</html>';
//echo utf8_decode($message);
$mail->From = 'soporte.dgie@correo.buap.mx';
$mail->FromName = utf8_decode('DGIE::Soporte técnico');
$mail->Subject = utf8_decode('Pregunta');
$mail->Body=utf8_decode($message);
$mail->AltBody = 'Su servidor de correo no soporta html';
$mail->AddAddress($correo);
$mail->AddBCC("adrian.lozada@correo.buap.mx");	
$mail->Send();
//start correo para soporte tecnico para dar seguimiento
$mail = new PHPMailer();	
$message = '<html>';
 //$message .='<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
$message .='<style type="text/css">
				.frase {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 8px;
				color: #989898;
				text-align: left;
				text-decoration: none;
			}
			.nombre {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
				font-weight: bold;
				color: #003B5C;
				text-align: left;
				margin: 0px;
			}
			.apellido {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 11px;
				font-weight: bold;
				color: #003B5C;
				text-align: left;
				margin: 0px;
			}
			.cargo {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 10px;
				font-style: oblique;
				color: #003B5C;
				line-height: 10px;
				text-align: left;
				vertical-align: middle;
				padding: 0px;
				margin-top: 5px;
				margin-right: 0px;
				margin-bottom: 0px;
				margin-left: 0px;
			}
			.bold {
				font-family: Arial, Helvetica, sans-serif;
				font-weight: bold;
				color: #003B5C;
				text-align: left;
				font-size: 10.5px;
			}
			.texto {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 10px;
				line-height: 12px;
				color: #003B5C;
				text-align: left;
				margin: 0px;
			}
			.informacion {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 11px;
				color: #003B5C;
				text-align: left;
				vertical-align: middle;
				margin: 0px;
				padding: 0px;
			}
			a {
				text-decoration: none;
			}
			.link {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 11px;
				color: #003B5C;
				text-align: left;
				text-decoration: underline;
			}
			</style>';
$message .='<body>';
$message .='<strong><span class="nombre">Soporte técnico:</span> </strong> <br><br>';
$message .='<span class="nombre">'.$comentario.'</span>';
$message .='<p>&nbsp;</p>';

 //startfirma
$message .='
            <table width="600" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td height="35" colspan="6" class="frase" style="border-top:1px solid #CCCCCC"><hr /></td>
			  </tr>
			  <tr>
			    <td width="87" rowspan="4" style="padding: 0; vertical-align:top"><a href="http://www.buap.mx/" target="_blank"><img src="http://dgie.buap.mx/firma/imagenes/buap.png" width="83" height="90" alt="BUAP" style="margin: 0"/></a></td>
			    <td colspan="2" rowspan="3" valign="middle" style="border-right:1px solid #CCCCCC; padding: 0 10px 0 0" ><span class="nombre">Soporte técnico</span><br />
			    <p class="cargo">Administración de Sistemas</p></td>
			    <td width="29" rowspan="3" ><img src="http://dgie.buap.mx/firma/imagenes/iconos.gif" width="19" height="67" alt="Contactos" style="margin-left: 5px; margin-right: 5px" /></td>
			    <td height="22" colspan="2" style="padding: 0"><a href="mailto:soporte.dgie@correo.buap.mx" class="texto">soporte.dgie@correo.buap.mx</a><span class="frase"> | </span></td>
			  </tr>
			  <tr>
			    <td height="22" colspan="2" valign="middle" class="informacion" style="padding: 0"><a href="tel:2222295500" class="texto">(222) 2 29 55 00</a><span class="frase"> | </span> <span class="informacion">Ext:  7902</span></td>
			  </tr>
			  <tr>
			    <td width="145" height="22" valign="middle" style="padding: 0;"><span class="link"><a href="http://www.dgie.buap.mx/soporte/" target="_blank" class="texto">http://dgie.buap.mx/soporte/</a></span><span class="frase"> |</span></span></td>
			    <td width="201" valign="middle" class="texto" style="padding: 0;"><a href="https://www.facebook.com/BUAPaDistancia" target="_blank"><img src="http://dgie.buap.mx/firma/imagenes/facebook.gif" alt="Facebook - BUAPaDistancia" width="25" height="17" /></a><a href="https://twitter.com/BUAPaDistancia" target="_blank"><img src="http://dgie.buap.mx/firma/imagenes/twitter.gif" alt="Twitter - BUAPaDistancia" width="25" height="17" /></a></td>
			  </tr>
			  <tr style="margin-top: 5" valign="top">
			    <td width="30" style="padding: 5px 0 0 0"><a href="http://www.dgie.buap.mx/" target="_blank"><span class="bold">dgie</span><span class="frase"> |</span></a></td>
			    <td width="108" class="texto" style="padding: 5px 0 0 0"><a href="http://www.dgie.buap.mx/" target="_blank" class="texto">Dirección General de Innovación Educativa</a></td>
			    <td colspan="3" class="texto" style="padding: 5px 0 0 10px">Lunes a Viernes de 9:00 - 16:00 hrs.<span class="frase"> | </span>Edif. 1DGIE, Av. San Claudio y 22 Sur, s/n, Cuidad Universitaria. Puebla, Pue. C.P. 72570 </td>
			  </tr>
			  <tr>
			    <td height="13px" colspan="6" class="frase" style="border-bottom: 1px solid #CCCCCC"><hr /><p><strong>ADVERTENCIA DE PRIVACIDAD. </strong>La información contenida en este correo electrónico y archivos adjuntos, es privada, confidencial, y para uso exclusivo del destinatario, por lo que la utilización indebida de la misma será responsabilidad directa de quien lo realice. Si Usted no es el destinatario y recibió éste por error, evite su utilización, reproducción, o difusión, debiendo eliminarlo de su computadora o cualquier dispositivo electrónico y comunicarlo inmediatamente a su emisor, quien goza de las salvaguardas previstas por los artículos 1, 2 fracción VI, 25 y 27 apartado A, fracción IV, de la Ley de Protección de Datos Personales en Posesión de los Sujetos Obligados del Estado de Puebla, y de las políticas y lineamientos en materia de datos personales del Estado de Puebla, y sus correlativos de la Federación, incluyendo los aplicables a entidades privadas.
			    <hr />
			    <strong>STATEMENT OF CONFIDENTIALITY.</strong> The information contained in this e-mail and its attached files is private and confidential, and for sole use by the recipient. The improper use of such information will be the direct responsibility of the person using it. If you are not the intended recipient and you received this e-mail, avoid its use, reproduction or disclosure. You should delete it from your computer or any electronic device and immediately inform the sender, who has the safeguards of Articles 1, 2 section VI, 25 and 27, paragraph A, section IV of the Personal Data Protection in Possession of Bound Subjects Act of the State of Puebla, and of the policies and guidelines in personal information of the State of Puebla, and its related articles in federal law, including those applicable to private entities. <p></p>
			    </td>
			  </tr>
			</table>';
 //endfirm
$message .= '</body>';
$message .= '</html>';
//echo utf8_decode($message);
$mail->From = $correo;
$mail->FromName = utf8_decode($nombre);
$mail->Subject = utf8_decode($asunto);
$mail->Body=utf8_decode($message);
$mail->AltBody = 'Su servidor de correo no soporta html';
$mail->AddAddress('adrian_lozada@hotmail.com');
$mail->AddBCC("adrian.lozada@correo.buap.mx");	
$mail->Send();
//fin correo para soporte tecnico para dar seguimiento