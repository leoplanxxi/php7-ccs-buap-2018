<!DOCTYPE html>
<html lang="en">
<?Php
$faq=base64_encode('faq');
$option=base64_encode('faq');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
switch (base64_decode($_GET[categoria])) {
  case 9:
    $header='single-page-header-faq-moodle';
    $categoria='Moodle';
    break;
  case 10:
    $header='single-page-header-faq-blackboard';
    $categoria='Blackboard';
    break;
  case 11:
    $header='single-page-header-faq-alumnosb';
    $categoria='Alumnos - BUAP';
    break;
  case 13:
    $header='single-page-header-faq-alumnosb';
    $categoria='Proceso de Admisión: Modalidades Semiescolarizada y a Distancia';
    break;
}
require_once 'funcion/funciones.php';

?>
<!-- Start single page header -->
  <section id="<?Php echo $header;?>">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Preguntas frecuentes / <?Php echo $categoria;?></h2>
              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>-->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li><a href="faq.php">Preguntas frecuentes</a></li>
                <li><a href="#"><?Php echo $categoria;?></a></li>
                <li class="active"> <?Php echo utf8_encode($article[0]['subject']);?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
   <!-- Start Pricing table -->
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title">Preguntas Frecuentes: <?Php echo $categoria;?></h2>
            <span class="line"></span>
            <!--<p>Haz clic en el video correspondiente</p>-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content">
            <div class="row">
              <table id="faqcategoria" class="display" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>Pregunta frecuente </th>
                      
                  </tr>
              </thead>
              <tfoot>
                  <tr>
                      <th> Pregunta frecuente </th>
                                    
                  </tr>
              </tfoot>
              <?Php 
                  $obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
                  $sql='SELECT 
                        kbarticlelinkid, swkbarticles.kbarticleid, subject, swkbarticles.views
                      FROM 
                        swkbarticles, swkbarticlelinks 
                      WHERE 
                        swkbarticlelinks.kbarticleid = swkbarticles.kbarticleid AND
                        swkbarticlelinks.linktypeid=:idCat ORDER BY swkbarticles.views';
                  $obj->query($sql);          
                  $obj->bind(':idCat', base64_decode($_GET[categoria]));
                  $faqs=$obj->resultset();
                  $obj->null;
              ?>
              <tbody>
                 <?Php 
                  foreach($faqs as $dfaqs)
                  {
                    ?>
                    <tr>
                      <td>
                        <div class="blog-news-title">
                          <h2> 
                            <a href="question.php?categoria=<?Php echo $_GET[categoria];?>&articulo=<?Php echo base64_encode($dfaqs['kbarticleid']);?>">
                            <?Php echo utf8_encode($dfaqs['subject']);?>
                            </a>
                          </h2>
                          <?Php 
                          $article=Buscaquestion(base64_encode($dfaqs['kbarticleid']));
                          echo utf8_encode(substr($article[0]['contentstext'], 0, 200));
                          ?>
                        </div> 
                      </td>
                    </tr>
                    <?Php
                  }
                 ?>
              </tbody>
          </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Pricing table --> 
<?Php
require_once 'footer/footer_v1.php';
?>
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.dataTables.min.js"></script>  
<script type="text/javascript">
      $(document).ready(function() {
        $('#faqcategoria').DataTable( {
            "paging":   true,
            "ordering": false,
            "info":     false
        } );
    } );
    </script>  
</body>
</html>