<?Php
function Buscaquestion($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
				$sql='SELECT 
					    	subject, dateline, contents, fullname, contentstext 
						FROM 
						  	swkbarticles, swkbarticledata, swstaff 
						WHERE 
						  	swkbarticles.kbarticleid = swkbarticledata.kbarticleid AND
						  	swkbarticles.creatorid=swstaff.staffid AND
						  	swkbarticles.kbarticleid=:id ';
				$obj->beginTransaction();
				$obj->query($sql);
				$obj->bind(':id', base64_decode($data));				
				$article=$obj->resultset();;
				$obj->endTransaction();
				$obj=null;
				return $article;
			}
	}
function BComentsQuestion($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
				$sql='SELECT 
					    	 fullname , dateline, swcommentdata.contents, swcomments.creatorid
						FROM 
						  	swcomments, swcommentdata 
						WHERE 						
						  	swcomments.typeid=:id AND
						  	swcommentdata.commentid=swcomments.commentid AND
						  	commentstatus!=3 
						ORDER BY dateline DESC';
				$obj->beginTransaction();
				$obj->query($sql);
				$obj->bind(':id', base64_decode($data));				
				$comment=$obj->resultset();;
				$obj->endTransaction();
				$obj=null;
				return $comment;
			}
	}
function setComments($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
				$valores=$data;
				$sql='INSERT INTO swcomments(typeid, 
				                             creatortype, 
				                             creatorid, 
				                             commenttype, 
				                             fullname, 
				                             email, 
				                             ipaddress, 
				                             dateline, 
				                             parentcommentid, 
				                             commentstatus, 
				                             useragent, 
				                             referrer, 
				                             parenturl)
				                       	VALUES(:typeid, 
				                             :creatortype, 
				                             :creatorid, 
				                             :commenttype, 
				                             :fullname, 
				                             :email, 
				                             :ipaddress, 
				                             :dateline, 
				                             :parentcommentid, 
				                             :commentstatus, 
				                             :useragent, 
				                             :referrer, 
				                             :parenturl)';
				$obj->beginTransaction();
				$obj->query($sql);		
				$obj->bind(':typeid', $valores['typeid']);
				$obj->bind(':creatortype', $valores['creatortype']);
				$obj->bind(':creatorid', $valores['creatorid']);
				$obj->bind(':commenttype', $valores['commenttype']);
				$obj->bind(':fullname', $valores['fullname']);
				$obj->bind(':email', $valores['email']);
				$obj->bind(':ipaddress', $valores['ipaddress']);
				$obj->bind(':dateline', $valores['dateline']);
				$obj->bind(':parentcommentid', $valores['parentcommentid']);
				$obj->bind(':commentstatus', $valores['commentstatus']);
				$obj->bind(':useragent', $valores['useragent']);
				$obj->bind(':referrer', $valores['referrer']);
				$obj->bind(':parenturl', $valores['parenturl']);				
				$obj->execute();
				$id=$obj->lastInsertId();		
				$obj->endTransaction();
				$obj=null;		
				return $id;		
				
			}
	}
function setCommentsData($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
				$valores=$data;
				$sql="INSERT INTO swcommentdata(commentid, contents) VALUES(:commentid, :contents)";
				$obj->beginTransaction();
				$obj->query($sql);	
				$obj->bind(':commentid', $valores['commentid']);
				$obj->bind(':contents', $valores['contents']);
				$obj->execute();
				$obj->endTransaction();
				$obj=null;	
			}
	}
function getRealIP()
{

	if (isset($_SERVER["HTTP_CLIENT_IP"]))
	{
		return $_SERVER["HTTP_CLIENT_IP"];
	}
	elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
	{
		return $_SERVER["HTTP_X_FORWARDED_FOR"];
	}
	elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
	{
		return $_SERVER["HTTP_X_FORWARDED"];
	}
	elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
	{
		return $_SERVER["HTTP_FORWARDED_FOR"];
	}
	elseif (isset($_SERVER["HTTP_FORWARDED"]))
	{
		return $_SERVER["HTTP_FORWARDED"];
	}
	else
	{
		return $_SERVER["REMOTE_ADDR"];
	}

}
function AllCategory()
	{
		$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
		$sql='SELECT 
				*
	    	  FROM 
				categorias';
		$obj->beginTransaction();
		$obj->query($sql);
		$obj->execute();
		$category=$obj->resultset();
		$obj->endTransaction();
		$obj=null;
		return $category;
	}
function searchCategory($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$sql='SELECT 
				      	nomCat, imgCat 
	    	          FROM 
						categorias
					  WHERE
					  	idCat=:idCat';
				$obj->beginTransaction();
				$obj->query($sql);
				$obj->bind(':idCat', base64_decode($data));				
				$category=$obj->resultset();;
				$obj->endTransaction();
				$obj=null;
				return $category;
			}
	}
	function totalCategory($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$sql='SELECT 
						count(*) as total
	    	          FROM 
						list_reprod
					  WHERE
					  	idCat=:idCat';
				$obj->beginTransaction();
				$obj->query($sql);
				$obj->bind(':idCat', base64_decode($data));				
				$category=$obj->resultset();;
				$obj->endTransaction();
				$obj=null;
				return $category;
			}
	}
function searchList($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$sql='SELECT 
				      	videos.idVideo, titulo, descripcion, autor, duracion, imagen 
	    	          FROM 
						list_reprod, videos
					  WHERE
					  	list_reprod.idVideo=videos.idVideo AND
					  	list_reprod.idCat=:idCat';
				$obj->beginTransaction();
				$obj->query($sql);
				$obj->bind(':idCat', base64_decode($data));				
				$videos=$obj->resultset();;
				$obj->endTransaction();
				$obj=null;
				return $videos;
			}
	}
function searchVideo($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$sql='SELECT 
				      	videos.idVideo, titulo, descripcion, autor, fpublicacion, visitas, mam, ma, map, nma, sc
	    	          FROM 
						videos
					  WHERE
					  	idVideo=:idVideo';
				$obj->beginTransaction();
				$obj->query($sql);
				$obj->bind(':idVideo', $data);				
				$video=$obj->resultset();;
				$obj->endTransaction();
				$obj=null;
				return $video;
			}
	}
function searchRating($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$sql='SELECT 
				      	 mam, ma, map, nma, sc
	    	          FROM 
						videos
					  WHERE
					  	idVideo=:idVideo';
				$obj->beginTransaction();
				$obj->query($sql);
				$obj->bind(':idVideo', $data);				
				$video=$obj->resultset();;
				$obj->endTransaction();
				$obj=null;
				return $video;
			}
	}
function setVistas($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$valores=$data;
				$sql="UPDATE videos SET visitas=:visitas WHERE idVideo=:idVideo";
				$obj->beginTransaction();
				$obj->query($sql);	
				$obj->bind(':visitas', $valores['visitas']);
				$obj->bind(':idVideo', $valores['idVideo']);
				$obj->execute();
				$obj->endTransaction();
				$obj=null;	
			}
	}
function setmam($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$valores=$data;
				$sql="UPDATE videos SET mam=:mam WHERE idVideo=:idVideo";
				$obj->beginTransaction();
				$obj->query($sql);	
				$obj->bind(':mam', $valores['mam']);
				$obj->bind(':idVideo', $valores['idVideo']);
				$obj->execute();
				$obj->endTransaction();
				$obj=null;	
			}
	}
function setma($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$valores=$data;
				$sql="UPDATE videos SET ma=:ma WHERE idVideo=:idVideo";
				$obj->beginTransaction();
				$obj->query($sql);	
				$obj->bind(':ma', $valores['ma']);
				$obj->bind(':idVideo', $valores['idVideo']);
				$obj->execute();
				$obj->endTransaction();
				$obj=null;	
			}
	}
function setsc($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$valores=$data;
				$sql="UPDATE videos SET sc=:sc WHERE idVideo=:idVideo";
				$obj->beginTransaction();
				$obj->query($sql);	
				$obj->bind(':sc', $valores['sc']);
				$obj->bind(':idVideo', $valores['idVideo']);
				$obj->execute();
				$obj->endTransaction();
				$obj=null;	
			}
	}
function setmap($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$valores=$data;
				$sql="UPDATE videos SET map=:map WHERE idVideo=:idVideo";
				$obj->beginTransaction();
				$obj->query($sql);	
				$obj->bind(':map', $valores['map']);
				$obj->bind(':idVideo', $valores['idVideo']);
				$obj->execute();
				$obj->endTransaction();
				$obj=null;	
			}
	}
function setnma($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$valores=$data;
				$sql="UPDATE videos SET nma=:nma WHERE idVideo=:idVideo";
				$obj->beginTransaction();
				$obj->query($sql);	
				$obj->bind(':nma', $valores['nma']);
				$obj->bind(':idVideo', $valores['idVideo']);
				$obj->execute();
				$obj->endTransaction();
				$obj=null;	
			}
	}
function BComentsVideos($data)
	{
		if(!empty($data))
			{
				
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$sql='SELECT 
					    	 fullname , dateline, commentdata.contents, comments.creatorid
						FROM 
						  	comments, commentdata 
						WHERE 						
						  	comments.idVideo=:idVideo AND
						  	commentdata.idComment=comments.idComment
						ORDER BY dateline DESC';
				$obj->beginTransaction();
				$obj->query($sql);
				$obj->bind(':idVideo', $data);				
				$comment=$obj->resultset();;
				$obj->endTransaction();
				$obj=null;
				return $comment;
			}
	}

function setCommentsV($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$valores=$data;
				$sql='INSERT INTO comments(idVideo, 
				                             creatorid, 
				                             fullname,
				                             email,
				                             ipaddress,
				                             dateline,
				                             parentcommentid,
				                             commentstatus,
				                             useragent,
				                             referrer,
				                             parenturl)
				                       	VALUES(:idVideo, 
				                             :creatorid, 
				                             :fullname,
				                             :email,
				                             :ipaddress,
				                             :dateline,
				                             :parentcommentid,
				                             :commentstatus,
				                             :useragent,
				                             :referrer,
				                             :parenturl)';
				$obj->beginTransaction();
				$obj->query($sql);		
				$obj->bind(':idVideo', $valores['idVideo']);
				$obj->bind(':creatorid', $valores['creatorid']);
				$obj->bind(':fullname', $valores['fullname']);
				$obj->bind(':email', $valores['email']);
				$obj->bind(':ipaddress', $valores['ipaddress']);
				$obj->bind(':dateline', $valores['dateline']);			
				$obj->bind(':parentcommentid', $valores['parentcommentid']);
				$obj->bind(':commentstatus', $valores['commentstatus']);
				$obj->bind(':useragent', $valores['useragent']);
				$obj->bind(':referrer', $valores['referrer']);
				$obj->bind(':parenturl', $valores['parenturl']);
				$obj->execute();
				$id=$obj->lastInsertId();		
				$obj->endTransaction();
				$obj=null;		
				return $id;		
				
			}
	}
function setCommentsDataV($data)
	{
		if(!empty($data))
			{
				$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
				$valores=$data;
				$sql='INSERT INTO commentdata(idComment, contents) VALUES(:idComment, :contents)';
				$obj->beginTransaction();
				$obj->query($sql);	
				$obj->bind(':idComment', $valores['idComment']);
				$obj->bind(':contents', $valores['contents']);
				$obj->execute();	
				$obj->endTransaction();
				$obj=null;		
			}
	}
function AllMoreView()
	{
		$obj = new Database(DB_HOST,DB_VIDEODGIE,USER_VIDEODGIE,PASS_VIDEODGIE);
		$sql='SELECT 
				visitas, categorias.idCat, nomCat, videos.idVideo, titulo, autor, duracion
	    	  FROM 
				categorias, videos, list_reprod
				WHERE 
				list_reprod.idVideo=videos.idVideo AND
				categorias.idCat=list_reprod.idCat ORDER BY visitas DESC LIMIT 10';
		$obj->beginTransaction();
		$obj->query($sql);
		$obj->execute();
		$category=$obj->resultset();
		$obj->endTransaction();
		$obj=null;
		return $category;
	}
?>
