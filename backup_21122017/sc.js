$(document).ready(function (e) {
	$("#fsc").on('submit',(function(e) {
		e.preventDefault();
		$.ajax({
        	url: "sc.php",   	// Url to which the request is send
			type: "POST",      				// Type of request to be send, called as method
			data:  new FormData(this), 		// Data sent to server, a set of key/value pairs representing form fields and values 
			contentType: false,       		// The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
    	    cache: false,					// To unable request pages to be cached
			processData:false,  			// To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
			success: function(data)  		// A function to be called if request succeeds
		    	{
					$("#tsc").empty(); 
					$("#tsc").html(data);			
		    	}	        
	   });
	}));
});
