<!DOCTYPE html>
<html lang="es">
<?Php
$novedades=base64_encode('novedadesbb');
$option=base64_encode('novedadesbb');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!--	<link rel="stylesheet" href="<?Php echo DEFAULT_LAYOUT;?>/css/lista-desplegable.css">-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Start single page header -->
  <section id="single-page-header-novedades">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Novedades Bb / APP Facilitadores</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active"><a href="novedades_bb.php">Novedades Bb</a></li>
				<li class="active">APP Facilitadores</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- End single page header -->
	
<!-- Inicio del contenido de la página-->
 <section id="novedades">
     <div class="contenedor appfacilitadores">
		 <section class="novedades-encabezado">
			 <div class="novedades-encabezado-interior">
				 <h1>Novedades</h1>
				 <h2>Plataforma Instruccional Institucional</h2>
				 <img src="templates/frontend/images/novedades-blackboard/logotipo-blackboard.png" style="height: 57px; width: 253px;" class="novedades-encabezado blackboard" alt="Blackboard">
			 </div>
			
			 	
				 <div class="app interior-facilitadores">
					 <img src="templates/frontend/images/novedades-blackboard/icono-facilitadores.png" style="height: 254px; width: 234px;" class="icono-facilitador" alt="App Facilitadores">
					 <p class="titulo-app">APP <br>Facilitadores</p>
				 </div>
				 
			 <div class="contenedor-listado">
			 	 <!--<ul lang="es" class="auto">
				 	<li>Contar con su listado de cursos.</li>
					 <li>Obtener una vista preliminar de elementos, actividades y pruebas.</li>
					 <li>Participar en debates.</li>
					 <li>Enviar anuncios.</li>
					 <li>Generar contenido.</li>
					 <li>Interactuar con su clase en <br><span class="collaborate">Blackboard Collaborate.</span></li>
				 </ul>-->
				 <ul class="contenedor-listado-ul">
  <li class="listado">
    <input type="checkbox" checked>
    <i></i>
    <h2  class="titulos-listado">Contar con su listado de cursos.</h2>
    <p class="parrafo">Con esta función el facilitador puede visualizar todos los cursos del periodo.</p>
	  <p class="parrafo"><img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg" style="width: 640px; height: 1136px;" class="zoom-img"></p>
	  
  </li>
  <li class="listado">
    <input type="checkbox" checked>
    <i></i>
    <h2  class="titulos-listado">Obtener una vista preliminar de elementos, actividades y pruebas.</h2>
    <p class="parrafo"></p>
  </li>
  <li class="listado" id="target">
    <input type="checkbox" checked>
    <i></i>
    <h2 class="titulos-listado">Participar en debates.</h2>
    <p class="parrafo">El facilitador puede interactuar en los foros disponibles dentro de sus cursos.</p>
	  <!--<p><embed src="https://indd.adobe.com/view/e2330531-7480-4dda-9a6c-fce874b00cf5"></p>-->
	  <p class="video-contenedor">
	  	<!--<object id="EdgeID" type="text/html" width="820" height="920" data-dw-widget="Edge" data="facilitador-participar-en-debates/Assets/facilitador-participar-en-debates.html">
  	    </object>-->
		  <video width="800px" height="900px" autoplay loop class="video">
		  	<source src="templates/frontend/videos/facilitador-instalacion-android_13.mp4" type="video/mp4">
			  Para ver el video con las instrucciones da click en el siguiente link:
		  </video>
		  
      </p>
  </li>
<li class="listado">
    <input type="checkbox" checked>
    <i></i>
    <h2 class="titulos-listado">Enviar anuncios.</h2>
    <p class="parrafo">By making the open state default for when :checked isn't detected, we can make this system accessable for browsers that don't recognize :checked. The fallback is simply an open accordion. The accordion can be manipulated with Javascript (if needed) by changing the "checked" property of the input element.</p>
  </li>
<li class="listado">
    <input type="checkbox" checked>
    <i></i>
    <h2 class="titulos-listado">Generar contenido.</h2>
    <p class="parrafo">By making the open state default for when :checked isn't detected, we can make this system accessable for browsers that don't recognize :checked. The fallback is simply an open accordion. The accordion can be manipulated with Javascript (if needed) by changing the "checked" property of the input element.</p>
  </li>
<li class="listado">
    <input type="checkbox" checked>
    <i></i>
    <h2 class="titulos-listado">Interactuar con su clase en <br><span class="collaborate">Blackboard Collaborate.</span></h2>
    <p class="parrafo">By making the open state default for when :checked isn't detected, we can make this system accessable for browsers that don't recognize :checked. The fallback is simply an open accordion. The accordion can be manipulated with Javascript (if needed) by changing the "checked" property of the input element.</p>
  </li>
</ul>
			 </div>
			 
		 </section>
	 </div>
	</section>
      
      <!-- Final del contenido de la página-->
      <?Php
require_once 'footer/footer_v1.php';
?>
</body>
</html>