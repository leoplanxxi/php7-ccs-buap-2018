<?Php 
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';

if(isset($_POST['g-recaptcha-response']))
	{

		$ipuser=$_SERVER['REMOTE_ADDR'];

		$recaptcha = $_POST["g-recaptcha-response"];

		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$data = array(
			'secret' => '6LcqNrQUAAAAAMv3JY88gLW1gJ0jhBccMKM1DuPF',
			'response' => $recaptcha,
			'remoteip' => $ipuser,
		);
		$options = array(
			'http' => array (
				'method' => 'POST',
				'content' => http_build_query($data)
			)
		);

		$context  = stream_context_create($options);
		$verify = file_get_contents($url, false, $context);
		$captcha_success = json_decode($verify);
		$res = $captcha_success->success;
		
		if ($res) {
			if ($_SESSION["CSRFToken"] == $_POST["CSRFToken"]) {
				if(isset($_POST['nombre']) && isset($_POST['email']) && isset($_POST['asunto']) && isset($_POST['comentario']))
				{
					if( !empty($_POST['nombre']) && !empty($_POST['email']) && !empty($_POST['asunto']) && !empty($_POST['comentario']))
						{
							$nombre=$_POST['nombre'];
							$correo=$_POST['email'];
							$comentario=$_POST['comentario'];
							$asunto=$_POST['asunto'];
							
							$asunto="DCyTIC:: CCS :: Mesa de Servicios / asunto: ".$asunto;
							require_once('aplication/libs/correo/class.phpmailer.php');
							require_once('templates/mail/mcontacto.php');
							$contacto=base64_encode('contacto');
							$option=base64_encode('contacto');
							echo "<script language='JavaScript'>";
							echo "location = 'ver.php?option=$option&contacto=$contacto'";
							echo "</script>";	
						}
				}

			}
			else {
				echo "Token CSRF inválido. Intente nuevamente.";
			}
		}
		else {
			echo "CAPTCHA incorrecto. Favor de intentar nuevamente";
		}
	}
?>