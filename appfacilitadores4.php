<!DOCTYPE html>
<html lang="es">
<?Php
$novedades=base64_encode('novedadesbb');
$option=base64_encode('novedadesbb');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!--	<link rel="stylesheet" href="<?Php echo DEFAULT_LAYOUT;?>/css/lista-desplegable.css">-->
	<link rel="stylesheet" href="style_tabs_acordeon.css">
	<script src="js/index.js"></script>
<!-- Start single page header -->
  <section id="single-page-header-novedades">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Novedades Bb / APP Facilitadores</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active"><a href="novedades_bb.php">Novedades Bb</a></li>
				<li class="active">APP Facilitadores</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- End single page header -->

<!-- Inicio del contenido de la página-->
 <section>
	 <div class="container">
	 <div class="row">
				<div class="col-xs-12 col-md-4">

		 <section class="novedades-encabezado">
			 <a href="novedades_bb.php">
			 	<div class="novedades-encabezado-interior">
				 	<h1>Novedades</h1>
				 	<h2>Plataforma Instruccional Institucional</h2>
				 	<img src="templates/frontend/images/novedades-blackboard/logotipo-blackboard.png" style="height: 64px; width: 450px;" class="novedades-encabezado blackboard" alt="Blackboard">
			 	</div>
			</a>
			 <div class="contenedor-app-facilitadores-int">
				<img src="templates/frontend/images/novedades-blackboard/icono-facilitadores.png" style="height: 254px; width: 234px;" class="icono-facilitadores-titulo" alt="App Facilitadores">
				 <p class="titulo-appf-int">APP <br>Facilitadores</p>
			 </div>
			 
					</div>
			 </section>	

				 <!--<div class="app interior-facilitadores">
					 <img src="templates/frontend/images/novedades-blackboard/icono-facilitadores.png" style="height: 254px; width: 234px;" class="icono-facilitador" alt="App Facilitadores">
					 <p class="titulo-app">APP <br>Facilitadores</p>
				 </div> -->
			<div class="row">
				<div class="col-xs-12 col-md-4">
				 <div class="naccs">
				  <div class="grid">
				   <div class="gc gc--1-of-3 izquierda">
				    <div class="menu">
				     <div ><span class="light"></span><span>Cursos</span></div>
				     <div><span class="light"></span><span>Vista preliminar</span></div>
				     <div><span class="light"></span><span>Debates</span></div>
				    </div>
				   </div>
				   <div class="gc gc--2-of-3 derecha">
				    <ul class="nacc">
				     <li>
				      <div class="contenedor-imagen">
						  <p>Contar con su listado de cursos</p>
				       		<img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
				      </div>
				     </li>
				     <li>
				      <div class="contenedor-imagen">
						  <p>Obtener una vista preliminar de elementos, actividades y pruebas</p>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
				      </div>
				     </li>
				     <li>
				      <div class="contenedor-imagen">
						  <p class="texto-video">Participar en debates</p>
				        <video autoplay loop>
				          <source src="templates/frontend/videos/facilitador-instalacion-android_13.mp4" type="video/mp4">
				        </video>
				      </div>
				     </li>
				    </ul>
				   </div>
				  </div>
				 </div>
				</div>
			</div>
		 
	 </div>
		 </div>
	</section>

      <!-- Final del contenido de la página-->
      <?Php
require_once 'footer/footer_v1.php';
?>


</body>
</html>
