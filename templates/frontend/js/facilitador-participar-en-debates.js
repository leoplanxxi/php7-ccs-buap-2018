(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"facilitador_participar_en_debates_atlas_", frames: [[0,1891,421,86],[0,1397,421,492],[0,750,421,645],[0,0,421,748],[423,0,136,122]]}
];


// symbols:



(lib._1debatesmascara = function() {
	this.spriteSheet = ss["facilitador_participar_en_debates_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib._1debates = function() {
	this.spriteSheet = ss["facilitador_participar_en_debates_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib._1debatiendo = function() {
	this.spriteSheet = ss["facilitador_participar_en_debates_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib._3impacto = function() {
	this.spriteSheet = ss["facilitador_participar_en_debates_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.cursormano = function() {
	this.spriteSheet = ss["facilitador_participar_en_debates_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Interpolación4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.instance = new lib._1debatiendo();
	this.instance.parent = this;
	this.instance.setTransform(-210.5,-322.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-210.5,-322.5,421,645);


(lib.Interpolación3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.instance = new lib._1debatiendo();
	this.instance.parent = this;
	this.instance.setTransform(-0.5,-0.5,0.002,0.002);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-0.5,1,1);


(lib.Interpolación2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0066CC").ss(5.8,1,1).p("Egg4gGtMBBxAAAIAANbMhBxAAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-213.3,-45.8,426.7,91.7);


(lib.img3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.instance = new lib._3impacto();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.img3, new cjs.Rectangle(0,0,421,748), null);


(lib.cursormano_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.instance = new lib.cursormano();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.cursormano_1, new cjs.Rectangle(0,0,136,122), null);


(lib.img33 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa_1
	this.instance = new lib.img3();
	this.instance.parent = this;
	this.instance.setTransform(210.5,374,1,1,0,0,0,210.5,374);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.img33, new cjs.Rectangle(0,0,421,748), null);


// stage content:
(lib.facilitadorparticiparendebates = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// cursor-mano
	this.instance = new lib.cursormano_1();
	this.instance.parent = this;
	this.instance.setTransform(915,398,1,1,0,0,0,68,61);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:498.1},24,cjs.Ease.cubicIn).wait(1).to({regX:68.2,regY:61.1,scaleX:0.76,scaleY:0.76,x:481,y:382.5},9,cjs.Ease.quadIn).to({regX:68,regY:61,scaleX:1,scaleY:1,x:498.1,y:398},7).wait(13).to({y:523},10).wait(1).to({regX:68.2,regY:61.1,scaleX:0.76,scaleY:0.76,x:481,y:508.2},9,cjs.Ease.quadIn).to({regX:68,regY:61,scaleX:1,scaleY:1,x:498.1,y:523},7).wait(13).to({x:656},18,cjs.Ease.quadOut).wait(1));

	// img-3-2
	this.instance_1 = new lib.img33();
	this.instance_1.parent = this;
	this.instance_1.setTransform(400,450,1,1,0,0,0,210.5,374);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(82).to({_off:false},0).to({alpha:1},12).wait(19));

	// marco copia
	this.instance_2 = new lib.Interpolación2("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(400.5,482.6,1,1.314);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(64).to({_off:false},0).to({regY:-0.1,y:482.5,alpha:1},6,cjs.Ease.quadIn).wait(11).to({startPosition:0},0).to({_off:true},1).wait(31));

	// img2-debatiendo
	this.instance_3 = new lib.Interpolación4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(401,450);
	this.instance_3.alpha = 0;

	this.instance_4 = new lib.Interpolación3("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(191.5,128.5);
	this.instance_4.alpha = 0;

	this.instance_5 = new lib._1debatiendo();
	this.instance_5.parent = this;
	this.instance_5.setTransform(190,128);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_4},{t:this.instance_3,p:{x:401,y:450,alpha:0}}]},41).to({state:[{t:this.instance_3,p:{x:400.5,y:450.5,alpha:1}}]},13).to({state:[{t:this.instance_5}]},40).to({state:[]},1).wait(18));

	// marco
	this.instance_6 = new lib.Interpolación2("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(400.5,337);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(17).to({_off:false},0).to({alpha:1},6,cjs.Ease.quadIn).wait(18).to({startPosition:0},0).to({_off:true},1).wait(71));

	// debatiendo
	this.instance_7 = new lib._1debatesmascara();
	this.instance_7.parent = this;
	this.instance_7.setTransform(190,294);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(54).to({_off:true},1).wait(58));

	// img-1
	this.instance_8 = new lib._1debates();
	this.instance_8.parent = this;
	this.instance_8.setTransform(190,204);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(54).to({_off:true},1).wait(58));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(590,654,793,492);
// library properties:
lib.properties = {
	id: '28FA21B5A67A49C5AAE6D04184ED147F',
	width: 800,
	height: 900,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/facilitador_participar_en_debates_atlas_.png", id:"facilitador_participar_en_debates_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['28FA21B5A67A49C5AAE6D04184ED147F'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;