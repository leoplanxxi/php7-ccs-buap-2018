<!DOCTYPE html>
<html lang="en">
<?Php
$idc=$_GET["idc"];
$video=base64_encode('video');
$option=base64_encode('video');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once (APLICATION.'/../funcion/funciones.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
/*require_once 'funcion/funciones.php';*/
?>
<!-- Start single page header -->
  <section id="single-page-header-faq-videot">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Video tutoriales / Categoría: <?Php echo $nomCategoria;?></h2>
              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>-->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li>Video tutoriales</li>
                <li class="active">Categorías</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <!-- Start Pricing table -->
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
           <!-- <h2 class="title">Categoría: Los más vistos</h2> -->
            <span class="line"></span>
            <!--<p>Haz clic en el video correspondiente</p>-->
          </div>
        </div>
        <div class="col-md-12">
          <div class="our-team-content">
            <div class="row">
             <table id="moreview" class="display" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th> </th>
			                <th>Categoría</th>
			                <th>Descripción</th>
			                <?php //<th>Total de videos</th>   ?>
			            </tr>
			        </thead>
			        <tfoot>
			            <tr>
			                <th></th>
			                <th>Categoría</th>
			                <th>Descripción</th>
			                <?php //<th>Total de videos</th>		              ?>
			            </tr>
			        </tfoot>
			        <tbody>               
                        <tr>
                            <td></td>
                            <td>
                                <a href="category.php?category=Mg=="><img src="<?Php echo DEFAULT_LAYOUT;?>/images/cat/comunicacion_red_internet_telefonia.png" alt="Comunicación de red, Internet y telefonía"></a>
                            </td>
                            <td>
                                <a href="category.php?category=Mg==">Comunicación de red, Internet y telefonía</a>
                            </td>
                            
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <a href="category.php?category=MQ=="><img src="<?Php echo DEFAULT_LAYOUT;?>/images/cat/inst_soporte_software_antivirus_mail.png" alt="Instalación o soporte de software, antivirus y correo electrónico"></a>
                            </td>
                            <td>
                                <a href="category.php?category=MQ==">Instalación o soporte de software, antivirus y correo electrónico</a>
                            </td>
                            
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <a href="category_videos.php?idc=3"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/cat/soporte_tec_plataformas_virt_aprend.png" alt="Soporte técnico relacionado con Plataformas Virtuales de Aprendizaje"></a>
                            </td>
                            <td>
                                <a href="category_videos.php?idc=3">Soporte técnico relacionado con Plataformas Virtuales de Aprendizaje</a>
                            </td>
                            
                        </tr>
			        </tbody>
			    </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Pricing table --> 
  
<?Php
require_once 'footer/footer_v1.php';
?>
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.dataTables.min.js"></script>  
<script type="text/javascript">
      $(document).ready(function() {
		    $('#moreview').DataTable( {
		        "paging":   true,
		        "ordering": false,
		        "info":     false
		    } );
		} );
    </script>  
</body>
</html>