<?Php
/**
 * Created by PhpStorm.
 * User: DGIE-CAS
 * Date: 12/02/2016
 * Time: 11:20 AM
 */
define('BASE_URL', 'https://ccs.buap.mx/');
#define('BASE_URL', 'http://148.228.11.54/dite/soporte-dev/');
//ruta de las platillas se carga por primera vez el default
define('DEFAULT_LAYOUT', BASE_URL.'/templates/frontend');

define('APP_NAME', 'Soporte DGIE');
define('APP_SLOGAN', 'Soporte tecnico');
define('APP_COMPANY', 'Direción General de Innovavion Educativa');
//siga datos DB
//define('DB_HOST', '172.18.24.9');
define('DB_HOST', '172.23.1.94');
define('USER_VIDEODGIE', 'kayako_user');
define('PASS_VIDEODGIE', 'K4y@k0_');
define('DB_VIDEODGIE', 'videos_dgie');
//kayako datos DB
define('USER_KAYAKO', 'kayako_user');
define('PASS_KAYAKO', 'K4y@k0_');
define('DB_KAYAKO', 'kayako_fusion');

define('DB_CHAR', 'utf8');
define('HASH_KEY', 'Am9kPf14vKTR');
define('IDIOMA', 'es_ES');
define('TIMEZONA', 'America/Mexico_City');
define('PAIS', 'MEXICO');
define('TIEMPO_SESION', 600);
define('FACEBOOK', 'https://www.facebook.com/BUAPaDistancia');
define('TWITTER', 'https://twitter.com/BUAPaDistancia');
define('PINTEREST', 'https://es.pinterest.com/soportedgie/');
define('VERSION', '1.0');

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

session_start();

//removeCSRFToken();

if (empty($_SESSION['CSRFToken'])) {
    generateCSRFToken(64);
}

function generateCSRFToken($length) { // length = 32
    if (function_exists('mcrypt_create_iv')) {
        $_SESSION['CSRFToken'] = bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    } else {
        $_SESSION['CSRFToken'] = bin2hex(openssl_random_pseudo_bytes($length));
    }
}

function removeCSRFToken() {
    unset($_SESSION['CSRFToken']);
}

$CSRFToken = $_SESSION['CSRFToken'];
//var_dump($_SESSION);
?>
