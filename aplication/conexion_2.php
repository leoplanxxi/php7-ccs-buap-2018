<?Php
/**
 * User: Adrian Francisco Lozada Reboceño
 * Date: 12/02/2016
 * Time: 11:21 AM
 * Version: 1.0
 * Class: Conexion PDO
 */
class DBConnection {
	public function DBConnection($host, $dbname, $user, $pass) {
		try {
			$DBH = new PDO("mysql:host=".$host.";dbname=".$dbname, $user, $pass);
			$DBH->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			return $DBH;
		} catch(PDOException $e) {
			echo 'ERROR: ' . $e -> getMessage();
		}
	}
}
?>
