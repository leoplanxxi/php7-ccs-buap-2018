<?php 

if (!isset($_GET["q"])) { header("Location: https://ccs.buap.mx/catalogo_serviciosv1.php?q=0"); }

switch ($_GET["q"]) {
    case 0:
        $titulo = "Comunicación de red, <b>internet y telefonía</b>";
        break;
    case 1: 
        $titulo = "Instalación o soporte de software, <b>antivirus y correo electrónico</b>";
        break;
    case 2:
        $titulo = "Soporte relacionado con<br><b>servidores y sitios web</b>";
        break;
    case 3:
        $titulo = "Infraestructura de red y asesoría relacionada con <b>nuevas tecnologías de la información</b>";
        break;
    case 4:
        $titulo = "Soporte técnico relacionado con <b>plataformas virtuales de aprendizaje</b>";
        break;
    default:
        break;
}
?>
<style>
.titulo-secc-serv { font-weight: 100; text-align: right; color: #003b5c;}
a.colapsible-serv {
    width: 100%;
    display: block;
    padding-top: 10px;
    padding-bottom: 10px;
    border: 1px solid #efefef;
    cursor: pointer;
    font-weight: bold;
    color: #003b5c;
    text-decoration: none;
}
.colapsible {
    padding-left: 60px;
    border-left: 10px solid #6c9db9;
    color: #003b5c;
    padding-top: 30px;
    padding-bottom: 30px;
    text-align: justify;
    padding-right: 15px;
}
.expand_caret {
    transform: scale(1.6);
    margin-left: 8px;
    margin-top: -4px;
}
a[aria-expanded='false'].colapsible-serv > .expand_caret {
    transform: scale(1.6) rotate(-90deg);
}

a[aria-expanded='true'].colapsible-serv {
    background: #c9e5f3;
}
</style>

<h3 class="titulo-secc-serv"><?php echo $titulo; ?></h3>

<?php /* Obtener información de la DB */
$servicios = AllServCat($_GET["q"]+1);
?>

<?php foreach ($servicios as $s): ?>
<a data-toggle="collapse" data-target="#collapse-<?php echo $s["idservicios"]; ?>" class="colapsible-serv" aria-expanded="false"><div class="expand_caret caret"></div>&nbsp;&nbsp; <?php echo $s["nombre_servicio"]; ?></a>
<div id="collapse-<?php echo $s["idservicios"]; ?>" class="collapse colapsible">
<?php echo $s["desc_servicio"]; ?>
</div>
<?php endforeach; ?>