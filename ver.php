<!DOCTYPE html>
<html lang="en">
<?Php
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
if(isset($_GET['option']) && !empty($_GET['option']))
	{
		if(isset($_GET['faq']) && !empty($_GET['faq']))
			{
				$faq=$_GET['faq'];
				$option=$_GET['option'];
				$bandera='Preguntas frecuentes';
				$link='faq.php';
			}
		if(isset($_GET['contacto']) && !empty($_GET['contacto']))
			{
				$cont=$_GET['contacto'];
				$option=$_GET['option'];
				$bandera='Contáctanos';
				$link='contact.php';
			}
		if(isset($_GET['video']) && !empty($_GET['video']))
			{
				$video=$_GET['video'];
				$option=$_GET['option'];
				$bandera='Videos';
				$link='moreview.php';
			}
		
	}
else{
	echo "<script language='JavaScript'>";
	echo "location = 'index.php'";
	echo "</script>";
}
require_once 'menu/menu.php';
?>
<!-- Start single page header -->
  <section id="single-page-header-contacto">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>-->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li><a href="<?Php echo $link;?>"><?Php echo $bandera; ?></a></li>
                
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <section id="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-area">
            <h2 class="title"><?Php echo $bandera; ?></h2>
            <span class="line"></span>
          </div>
          <p>&nbsp;</p>
          <p><h4><strong>Hemos recibido tu incidente, un agente de nuestra Mesa de Servicios le dará seguimiento, adicionalmente se ha enviado a tu correo electrónico copia del incidente.</strong></h4></p>
        </div>
      </div>
    </div>
  </section>
  <!-- End Pricing table -->
  <?Php
require_once 'footer/footer_v1.php';
?>
</body>
</html