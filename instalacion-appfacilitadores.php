<!DOCTYPE html>
<html lang="es">
<?Php
$novedades=base64_encode('novedadesbb');
$option=base64_encode('novedadesbb');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!--	<link rel="stylesheet" href="<?Php echo DEFAULT_LAYOUT;?>/css/lista-desplegable.css">-->
	<link rel="stylesheet" href="style_tabs_acordeon.css">
	<script src="js/index.js">
	</script>
<!--	<script>
		var video = document.getElementById('video');
video.addEventListener('on',function(){
  video.play();
},false);
	</script>-->
<!--	<script>
function myFunction() {
    var x = document.getElementById("instalacion-facilitadores-android");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>-->
	<script type="text/javascript">
$(document).ready(function(){
  $("#mac").click(function(){
    $("#instalacion-facilitadores-android").hide();
	    $("#instalacion-facilitadores-mac").show();
  });
  $("#android").click(function(){
    $("#instalacion-facilitadores-android").show();
	    $("#instalacion-facilitadores-mac").hide();
  });
});
</script>

<!-- Start single page header -->
  <section id="single-page-header-novedades">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Novedades Bb / APP Facilitadores</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active"><a href="novedades-blackboard.php">Novedades Bb</a></li>
				<li class="active">Instalación APP Facilitadores</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- End single page header -->

<!-- Inicio del contenido de la página-->
  <section id="contact">
     <div class="container">
       <div class="row">
		   <a href="appfacilitadores.php">
		   <div class="col-xs-12 col-md-4 div-app-facilitadores">
		   		<img src="templates/frontend/images/novedades-blackboard/icono-facilitadores.png" style="height:auto;" class="icono-facilitadores" alt="App Facilitadores">
				<p class="titulo-app-fac">APP <br>Facilitadores</p>
		 </div>
		   </a>
		   <div class="col-md-2"></div>
         <div class="col-xs-12 col-md-6">
           <div class="title-area title-area-borde">
				 <h1 class="title instalacion">Instalación de la APP</h1>
				 <h2 class="subtitulo">Plataforma Instruccional Institucional</h2>
			   <center>
			   		<img src="templates/frontend/images/novedades-blackboard/logo-blackboard-mobile.png" class="logo-blackboard" alt="Blackboard">
			   </center>
            </div>
         </div>


       </div>

		<div class="row">
         <div class="col-xs-12 col-md-10 col-md-offset-1 col-xl-12 contenido-instalacion-facilitador">
			 <div class="row">
			 	 <div class="col-md-10 col-md-offset-1">
				 	<p class="novedades-texto-contenido instalacion-app">Con la <span class="text-facilitador"> Aplicación Blackboard Instructor</span> podrás visualizar cada uno de tus cursos.</p>
			 		<p class="novedades-texto-contenido instalacion-app">La APP está disponible para dispositivos móviles <span class="t_negro">iOS y Android.</span></p>
					<p class="novedades-texto-contenido instalacion-app">Para poder hacer uso de la APP, puedes seguir los siguientes pasos:</p>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
						<button class="boton-app-facilitador instalacion-android" id="android" href="#">
							<img class="logo-android" src="templates/frontend/images/novedades-blackboard/android-logo.png">
							<p>Instalación de la APP <span>Android</span></p>
					 	</button>
						<button class="boton-app-facilitador instalacion-iphone" id="mac" href="#">
							<img class="logo-android" src="templates/frontend/images/novedades-blackboard/iphone-logo.png">
							<p>Instalación de la APP <span>iPhone</span></p>
					 	</button>
						</div>
					<div class="col-md-10 col-md-offset-1">
						<a href='https://play.google.com/store/apps/details?id=com.blackboard.android.bbinstructor&hl=en&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1' target="_blank">
							<img alt='Disponible en Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/es_badge_web_generic.png' style="width:45%; float:left;" />
						</a>
						<a href="https://itunes.apple.com/us/app/blackboard-instructor/id1088457824?mt=8" target="_blank">
						  <img src="macappstore.svg" class="imgSVG" alt="imagen SVG" title="imagen SVG" style="width:45%; float:right;" />
						</a>
					</div>
				</div>
				 </div>
			 </div>

<!--			 	<video autoplay loop>
				      <source src="templates/frontend/videos/facilitador-instalacion-android_13.mp4" type="video/mp4">
				</video>-->



			 	<div id="instalacion-facilitadores-android" style="display: block;">

					<iframe class="iframe-instalacion" width="560" height="600" src="https://www.youtube.com/embed/NeH4yiQJsJE" gesture="media" allowfullscreen></iframe>
				</div>
			 	<div id="instalacion-facilitadores-mac" style="display: none;">
					<iframe class="iframe-instalacion" width="560" height="600" src="https://www.youtube.com/embed/238omZmD6zY" frameborder="0" gesture="media" allowfullscreen></iframe>
				</div>
         </div>
       </div>

     </div>
  </section>

<!-- Final del contenido de la página-->
      <?Php
require_once 'footer/footer_v1.php';
?>


</body>
</html>
