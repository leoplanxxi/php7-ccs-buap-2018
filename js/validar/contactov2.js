$(document).ready(function () {		
    $("#acceptTerms").jqxCheckBox({width: 300});
     $('#form').jqxValidator({
          rules: [
                 { input: '#nombre', message: 'Se requiere su nombre!', action: 'keyup, blur', rule: 'required' },
                 { input: '#nombre', message: 'Su nombre debe contener sólo letras!', action: 'keyup, blur', rule: 'notNumber' },
                 { input: '#email', message: 'Se requier su correo electrónico!', action: 'keyup, blur', rule: 'required' },				
                 {
                      input: '#email', message: 'Su Correo electrónico debe tener el siguiente formato ejemplo@exjemplo.com', action: 'keyup, blur', rule:
                    function (input) {
                        var email = /^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/;
                        return (email.test(input.val())==true);
                    }
                    },	
                    { input: '#asunto', message: 'Se requiere su asunto!', action: 'keyup, blur', rule: 'required' },
                  
                    { input: '#comentario', message: 'Se requiere su comentario!', action: 'keyup, blur', rule: 'required' },	
                    { input: '#acceptTerms', message: 'Neceista aceptar aviso de privacidad!', action: 'keyup, blur', rule: 'required'},				
                 ]
     });

       /*{ input: '#facultad', message: 'Se requiere su facultad!', action: 'keyup, blur', rule: 'required' },
                    
                    { input: '#dato_tipo_usuario', message: 'Se requiere su información!', action: 'keyup, blur', rule: 'required' },*/
     // validate form.
     $("#sendButton").click(function () {
         var validationResult = function (isValid) {
             if (isValid) {
                 $("#form").submit();
             }
         }
         $('#form').jqxValidator('validate', validationResult);
     });
     $("#form").on('validationSuccess', function () {
         $("#form-iframe").fadeIn('fast');
     });
 });