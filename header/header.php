<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Soporte t&eacute;cnico: <?Php echo base64_decode($option); ?></title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?Php echo DEFAULT_LAYOUT;?>/images/favicon.png"/>
    <link rel="stylesheet" href="<?Php echo DEFAULT_LAYOUT;?>/css/demo.css">
    <!-- Font Awesome -->
    <link href="<?Php echo DEFAULT_LAYOUT;?>/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?Php echo DEFAULT_LAYOUT;?>/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="<?Php echo DEFAULT_LAYOUT;?>/css/slick.css"/>
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="<?Php echo DEFAULT_LAYOUT;?>/css/jquery.fancybox.css" type="text/css" media="screen" />
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="<?Php echo DEFAULT_LAYOUT;?>/css/animate.css"/>
    <!-- Progress bar  -->
    <link rel="stylesheet" type="text/css" href="<?Php echo DEFAULT_LAYOUT;?>/css/bootstrap-progressbar-3.3.4.css"/>
    <!-- Theme color -->
    <link id="switcher" href="<?Php echo DEFAULT_LAYOUT;?>/css/theme-color/blue-dgie.css" rel="stylesheet">
    <!-- Main Style -->
    <link href="style-dgie.css" rel="stylesheet">
    <!--<link href="style-dgie2.css" rel="stylesheet">-->
    <!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Open Sans for body font -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?Php echo DEFAULT_LAYOUT;?>/css/jquery.dataTables.min.css">
    <!--<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>-->
    <!-- Lato for Title -->
    <!--<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- BEGAIN PRELOADER -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- END PRELOADER -->
<!-- SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<!-- END SCROLL TOP BUTTON -->
<!-- Start header -->
<header id="header">
    <!-- header top search -->
    <div class="header-top">
        <div class="container">
            <?php /*<form action="">
                <div id="search">
                    <input type="text" placeholder="Escriba su palabra clave de búsqueda aquí y pulsa Enter ..." name="s" id="m_search" style="display: inline-block;">
                    <button type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </form> */?>
        </div>
    </div>
    <!-- header bottom -->
    <div class="header-bottom">
        <div class="container">
            <div class="row">
				<div class="col-md-8">
					<img src="https://dcytic.buap.mx/sites/all/themes/plantilla_1/images/escudo_blanco.png">
				</div>
				<div class="col-md-4 col-links">
					<a href="http://www.autoservicios.buap.mx/" class="links" target="_blank">Autoservicios</a>
					<a href="http://www.correo.buap.mx/" class="links" target="_blank">Correo BUAP</a>
					<a href="https://facebook.com/BUAPOficial/" class="links-img" target="_blank"><img src="https://dcytic.buap.mx/sites/all/themes/plantilla_1/images/facebook.png"></a>
					<a href="https://twitter.com/BUAPOficial/" class="links-img" target="_blank"><img src="https://dcytic.buap.mx/sites/all/themes/plantilla_1/images/twitter.png"></a>
					<a href="https://instagram.com/BUAPOficial/" class="links-img" target="_blank"><img src="https://dcytic.buap.mx/sites/all/themes/plantilla_1/images/instagram.png"></a>
					<a href="https://youtube.com/user/ibuap/" class="links-img" target="_blank"><img src="https://dcytic.buap.mx/sites/all/themes/plantilla_1/images/youtube.png"></a>
				</div>
            </div>
        </div>
    </div>
	<div class="row">
	<div class="container">
		<div class="dep">Dirección General de Cómputo y Tecnologías de la Información y Comunicaciones</div>
	</div>
	</div>
</header>
<!-- End header -->