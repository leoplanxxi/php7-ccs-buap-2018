<!DOCTYPE html>
<html lang="en">
<?Php
$cont=base64_encode('contacto');
$option=base64_encode('contacto');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!-- Start single page header -->
  <section id="single-page-header-contacto">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2></h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active">Contáctanos</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <section id="contact">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <div class="title-area">
              <h2 class="title">¿Quieres reportar algún incidente?</h2>
              <span class="line"></span>            
            </div>
         </div>
         <div class="col-md-12">
           <div class="cotact-area">
             <div class="row">
               <div class="col-md-4">
                 <div class="contact-area-left">
                   <h4>Datos de contacto</h4>
                   <p>DCyTIC -  Dirección General de Cómputo y Tecnologías de la Información y Comunicaciones</p>
                   <address class="single-address">
 
                     <p><strong> Horario de atención:</strong>
                     </p>
                     <p>
                     	Lunes a viernes de 09:00 a 17:00 horas.<br>
						<a href="mailto:ccs@correo.buap.mx">ccs@correo.buap.mx</a><br>
						Tel. 222 2140700<br>
						Ext. 6789
					</p>
                   </address> 
                             
                 </div>
               </div>
          
          <link rel="stylesheet" href="js/jqwidgets/styles/jqx.base.css" type="text/css" />
				  <link rel="stylesheet" href="js/jqwidgets/styles/jqx.darkblue.css" type="text/css"/>
				  <script type="text/javascript" src="js/scripts/jquery-1.11.1.min.js"></script>
				  <script type="text/javascript" src="js/jqwidgets/jqxcore.js"></script>
				  <script type="text/javascript" src="js/jqwidgets/jqxexpander.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxvalidator.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxbuttons.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxcheckbox.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/globalization/globalize.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxcalendar.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxdatetimeinput.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxmaskedinput.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxlistbox.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxcombobox.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxscrollbar.js"></script> 
				  <script type="text/javascript" src="js/jqwidgets/jqxinput.js"></script>
				  <script type="text/javascript" src="js/jqwidgets/jqxdata.js"></script>    
				  <script type="text/javascript" src="js/scripts/demos.js"></script> 
				  <script type="text/javascript" src="js/validar/contacto.js"></script>
          <script src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang;?>" async defer></script>

				  
               <div class="col-md-8">
                 <div class="contact-area-right">
                   <form action="ecommet.php" class="comments-form contact-form" name="form" id="form" method="post">
                    <input type="hidden" name="CSRFToken" id="CSRFToken" value="<?php echo $_SESSION["CSRFToken"]; ?>">
                    <div class="form-group">                        
                      <input type="text" name="nombre" id="nombre" class="form-control" style="width: 525px" placeholder="Tu nombre">
                    </div>
                    <div class="form-group">                        
                      <input type="text" name="email" id="email"  class="form-control" style="width: 525px" placeholder="Correo electrónico">
                    </div>
                     <div class="form-group">                        
                      <input type="text" id="asunto" name="asunto" class="form-control" style="width: 525px" placeholder="Asunto">
                    </div>
                    <div class="form-group">                        
                      <textarea id="comentario" name="comentario"  placeholder="Incidente" style="width: 525px"  rows="3" class="form-control"></textarea>
                    </div>
                     <div class="form-group">
				        	<label> <div id="acceptTerms" name="acceptTerms"><strong><span class="text-danger"> Acepto que he leído el Aviso de Privacidad </span> </strong></div></label> 
				        </div>    
				      
			    <div class="form-group">
					<textarea name="aviso" style="text-align: justify" class="form-control input-mini" rows="3" cols="109"  readonly="readonly" >AVISO DE PRIVACIDAD: La Benem&eacute;rita Universidad Aut&oacute;noma de Puebla hace de su conocimiento que se recaban sus datos personales &uacute;nica y exclusivamente para el cumplimiento de los fines que legalmente tiene como organismo p&uacute;blico, y que se adoptan las medidas de seguridad administrativas, t&eacute;cnicas y f&iacute;sicas para garantizar el manejo y tratamiento adecuado de los mismos. Por tanto, ponemos a su disposici&oacute;n el Aviso de Privacidad Institucional en Extenso en la direcci&oacute;n electr&oacute;nica www.transparencia.buap.mx para conocer los derechos y obligaciones que se derivan del presente...
			    	</textarea>
				</div>
        <div class="g-recaptcha" id="reCaptcha" name="reCaptcha" data-sitekey="6LcqNrQUAAAAAPMdn5fGkswIPSO4XzUWtyoQDoMK"></div>
        <script type="text/javascript">
          function validateCaptcha(){
            if(grecaptcha.getResponse() == null){
                alert("Por favor completa el reCaptcha");
                return false;
            }
            return true;
          }
        </script>
        <br>
                    <input type="button" id="sendButton" class="comment-btn" value="Enviar"/>
                  </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
  </section>
  <!-- End contact section  -->
  <!-- Start google map -->
  <?php /*<section id="google-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3772.3956452702696!2d-98.20053468466025!3d19.002279859222526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfbf608de241cf%3A0x2a975de83ee53662!2sDirecci%C3%B3n+General+de+Innovaci%C3%B3n+Educativa+de+la+BUAP!5e0!3m2!1ses!2smx!4v1453222810508" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </section> */ ?>
  <!-- End google map -->
<?Php
require_once 'footer/footer_v1.php';
?>
</body>
</html>