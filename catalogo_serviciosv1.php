<!DOCTYPE html>
<html lang="en">
<?Php
$cont=base64_encode('contacto');
$option=base64_encode('contacto');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once (APLICATION.'/../funcion/funciones.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<style>

.row-servicios .col-serv { padding-bottom: 25px; margin-bottom: 20px; border-bottom: 5px solid rgba(0, 204, 255, 0.3); }
.row-servicios .img-servicio { width: 50%; float: left; text-align: center;}
.row-servicios .img-servicio img { height: 55px; width: auto; text-align: center; }
</style>

  <section id="contact">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <div class="title-area">
              <h2 class="title" style="font-weight: lighter; color: #003b5c;">Catálogo de Servicios</h2>
              <span class="line"></span>            
            </div>
         </div>
       </div>
       <p>&nbsp;</p>
       <div class="row row-servicios">
        <div class="col-md-6">
            <div class="col-md-12 col-serv">
                <a href="?q=0"><div class="img-servicio"><img src="<?php echo DEFAULT_LAYOUT;?>/images/cat/cats_cat/internet_telefonia.png" class="img-3iconos"></div>
                <div class="tag-servicio">Comunicación de red, <br><b>internet y telefonía</b></div>
                </a>
            </div>
            <div class="col-md-12 col-serv">
                <a href="?q=1"><div class="img-servicio"><img src="<?php echo DEFAULT_LAYOUT;?>/images/cat/cats_cat/antivirus_correo.png" class="img-3iconos"></div>
                <div class="tag-servicio">Instalación o soporte de software, <br><b>antivirus y correo electrónico</b></div>
                </a>
            </div>
            <div class="col-md-12 col-serv">
                <a href="?q=2"><div class="img-servicio"><img src="<?php echo DEFAULT_LAYOUT;?>/images/cat/cats_cat/servidores_web.png" class="img-1icono"></div>
                <div class="tag-servicio">Soporte relacionado con<br><b>servidores y sitios web</b></div>
                </a>
            </div>
            <div class="col-md-12 col-serv">
                <a href="?q=3"><div class="img-servicio"><img src="<?php echo DEFAULT_LAYOUT;?>/images/cat/cats_cat/nuevas_ti.png" class="img-2iconos"></div>
                <div class="tag-servicio">Infraestructura de red y <br>asesoría relacionada con <b>nuevas<br> tecnologías de la información</b></div>
                </a>
            </div>
            <div class="col-md-12 col-serv">
                <a href="?q=4"><div class="img-servicio"><img src="<?php echo DEFAULT_LAYOUT;?>/images/cat/cats_cat/plataformas_aprendizaje.png" class="img-1icono"></div>
                <div class="tag-servicio">Soporte técnico relacionado <br>con <b>plataformas virtuales<br>de aprendizaje</b></div>
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <?php include_once "catalogo_serviciosv1.catinfo.php"; ?>
        </div>
       </div>
     </div>
  </section>

<?Php
require_once 'footer/footer_v1.php';
?>
</body>
</html>