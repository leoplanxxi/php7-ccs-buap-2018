<!-- Start footer -->
<footer id="footer" style="padding: 35px 0;     background-color: rgb(0,59,92);">
<!-- Container: centra el contenido y agrega un margen -->
<style>
	.col-info-buap, .col-info-dcytic {
		color: #00c7f5; margin-left: 25px; font-size: 13px;
	}
	
	.col-links-footer ul a {
		color: #00c7f5; font-size: 13px;
	}
</style>
    <div class="container">
    	<!-- Row: agregar filas -->
        <div class="row">
        	<div class="col-md-2">
				<img src="https://dcytic.buap.mx/sites/all/themes/plantilla_1/images/logo_buap.png">
			</div>
            <div class="col-md-3 col-info-buap">
			<b>Benemérita Universidad Autónoma de Puebla</b><br>
			4 Sur 104 Centro Histórico C.P. 72000<br>
			Teléfono +52 (222) 229 55 00
			</div>
			<div class="col-md-3 col-links-footer">
				<ul>
					<li><a href="https://www.buap.mx/calendario-escolar" target="_blank">Calendario Escolar</a></li>
					<li><a href="https://www.buap.mx/calendario-escolar" target="_blank">Identidad Gráfica</a></li>
					<li><a href="https://www.buap.mx/directorio-telefonico" target="_blank">Directorio</a></li>
					<li><a href="http://www.comunicacion.buap.mx/" target="_blank">Prensa</a></li>	
					<li><a href="https://www.buap.mx/privacidad" target="_blank">Aviso de privacidad</a></li>
				</ul>
			</div>
			<div class="col-md-3 col-info-dcytic">
				<strong>Dirección General de Cómputo y Tecnologías de la Información y Comunicaciones</strong><br>
				<span style="font-size: 12px; color: #00B8E0">Tel. +52 222 2140700<br>Ext. 6789</span>
			</div>
        </div>
		
    </div>
</footer>
<!-- End footer -->
 <!-- jQuery library -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap -->
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/bootstrap.js"></script>
<!-- Slick Slider -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/slick.js"></script>
<!-- mixit slider -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.fancybox.pack.js"></script>
<!-- counter -->
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/waypoints.js"></script>
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.counterup.js"></script>
<!-- Wow animation -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/wow.js"></script>
<!-- progress bar   -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/bootstrap-progressbar.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/custom.js"></script>
  