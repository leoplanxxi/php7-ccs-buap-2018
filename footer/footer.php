<!-- Start footer -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="footer-left">
                    <div class="row">
                    	
                        <div class="col-md-5 col-sm-5" style="vertical-align: middle; margin-top: -5px; text-align:center">
                        <div style="padding-bottom:5px"><span style="font-size: 12px; color: #00B8E0; text-align:center; margin-bottom:5px;">Enlaces BUAP</span></div>
                        
                            <a href="#"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/amereiaf.png" /></a>
                            <a href="#"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/anuies.png" /></a>
                            <a href="#"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/contraloriasocial.png" /></a>
                            <a href="#"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/transparencia.png" /></a>
                            <br>
                            <a href="#"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/farmacias.png" /></a>
                            <a href="#"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/lobos.png" /></a>
                            <a href="#"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/sorteo.png" /></a>
                            <a href="#"><img src="<?Php echo DEFAULT_LAYOUT;?>/images/ccu.png" /></a>
                        </div>
                        <div class="col-md-5 col-sm-5" style="margin-top:-5px" >
                            <a href="#" style="font-size: 11px;color: #00B8E0">Aviso de Privacidad</a><br>
                            <a href="#" style="font-size: 11px;color: #00B8E0">Directorio Telefónico</a><br>
                            <a href="#" style="font-size: 11px;color: #00B8E0">Mapa del Sitio</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="footer-right">
                    <div class="row">
                        <div class="col-md-5 col-sm-5" style="vertical-align: middle">
            	<span style="font-size: 12px; color: #00B8E0">Tel. +52 222 2140700<br>Ext. 6789</span>
                        </div>
                        <div class="col-md-5 col-sm-5" >
                            <img src="<?Php echo DEFAULT_LAYOUT;?>/images/footer/escudo_bottom.png" style="margin-top:-25px"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End footer -->
 <!-- jQuery library -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap -->
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/bootstrap.js"></script>
<!-- Slick Slider -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/slick.js"></script>
<!-- mixit slider -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.fancybox.pack.js"></script>
<!-- counter -->
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/waypoints.js"></script>
<script src="<?Php echo DEFAULT_LAYOUT;?>/js/jquery.counterup.js"></script>
<!-- Wow animation -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/wow.js"></script>
<!-- progress bar   -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/bootstrap-progressbar.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="<?Php echo DEFAULT_LAYOUT;?>/js/custom.js"></script>
  