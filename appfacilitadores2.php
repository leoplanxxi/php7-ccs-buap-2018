<!DOCTYPE html>
<html lang="es">
<?Php
$novedades=base64_encode('novedadesbb');
$option=base64_encode('novedadesbb');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!-- Start single page header -->
  <section id="single-page-header-novedades">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Novedades Bb / APP Facilitadores</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active"><a href="novedades_bb.php">Novedades Bb</a></li>
				<li class="active">APP Facilitadores</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- End single page header -->
	
<!-- Inicio del contenido de la página-->
 <section id="novedades">
     <div class="contenedor appfacilitadores">
		 <section class="novedades-encabezado">
			 <div class="novedades-encabezado">
				 <h1>Novedades</h1>
				 <h2>Plataforma Instruccional Institucional</h2>
				 <img src="templates/frontend/images/novedades-blackboard/logotipo-blackboard.png" style="height: 57px; width: 253px;" class="novedades-encabezado blackboard" alt="Blackboard">
			 </div>
			
			 	
				 <div class="app interior-facilitadores">
					 <img src="templates/frontend/images/novedades-blackboard/icono-facilitadores.png" style="height: 254px; width: 234px;" class="icono-facilitador" alt="App Facilitadores">
					 <p class="titulo-app">APP <br>Facilitadores</p>
				 </div>
		</section>		 
			 <div class="listado-contenedor">
			 	 <ul lang="es" class="listado-facilitadores">
				 	<li class="lista">Contar con su listado de cursos.</li>
					 <li class="lista">Obtener una vista preliminar de elementos, actividades y pruebas.</li>
					 <li class="lista">Participar en debates.</li>
					 <li class="lista">Enviar anuncios.</li>
					 <li class="lista">Generar contenido.</li>
					 <li class="lista">Interactuar con su clase en <span class="collaborate">Blackboard Collaborate.</span></li>
				 </ul>
			 </div>
			 
		 
	 </div>
	</section>
      
      <!-- Final del contenido de la página-->
      <?Php
require_once 'footer/footer_v1.php';
?>
</body>
</html>