<!DOCTYPE html>
<html lang="en">
<?Php
$novedades=base64_encode('novedadesbb');
$option=base64_encode('novedadesbb');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!-- Start single page header -->
  <section id="single-page-header-novedades">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Novedades</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active">Novedades</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- End single page header -->
	
<!-- Inicio del contenido de la página-->
 <section id="novedades">
     <div class="contenedor">
		 <section class="novedades-encabezado">
			 <div class="novedades-encabezado">
				 <h1>Novedades</h1>
				 <h2>Plataforma Instruccional Institucional</h2>
				 <img src="templates/frontend/images/novedades-blackboard/logo-blackboard-mobile.png" class="novedades-encabezado blackboard" alt="Blackboard">
			 </div>
			 <p class="novedades-texto-contenido pcentrado">Para ampliar las formas de acceso, ya contamos con la <span class="t_negro">Aplicación de iOS y Android</span> <span class="t_azul_claro">¡Sin costo!.</span></p>
			 <p class="novedades-texto-contenido pcentrado"><span class="t_azul_obscuro">Ingresa con tus mismos datos de acceso.</span></p>
			 <div class="contenedor-fa">
			 <div class="facilitadores">
				 
			 <div class="novedades-contenido">
				 
				 <div class="app-facilitadores">
					
					 
					 <div class="app grow">
						 <a class="app" href="appfacilitadores.php"><img src="templates/frontend/images/novedades-blackboard/icono-facilitadores.png" style="height: 254px; width: 234px;" class="icono-facilitador" alt="App Facilitadores">
						 <p class="titulo-appf">APP <br>Facilitadores</p></a>
					 </div>
					 
				</div>
				 
			 </div>
			 <div class="instalacion-app-facilitador">
				 <img src="templates/frontend/images/novedades-blackboard/flecha-facilitador.png" class="flecha-app" alt="flecha hacia abajo" style="width: 14px; height: 37px;">
			 	<a class="boton-app-facilitador" href="instalacion-appfacilitadores.php">Instalación<br>de la APP</a>
			 </div>
			 </div>
			 
			 <div class="alumnos">
			 <div class="novedades-contenido">
				 
				 <div class="app-facilitadores">
					
					 
					 <!--<div class="app grow">
						 <a class="app" href="appalumnos.php"><img src="templates/frontend/images/novedades-blackboard/icono-alumnos.png" style="height: 254px; width: 234px;" class="icono-facilitador" alt="App Facilitadores">
						 <p class="titulo-app">APP <br>Alumnos</p></a>
					 </div>-->
					 <div class="app grow">
						 <a class="app" href="appalumnos.php"><img src="templates/frontend/images/novedades-blackboard/icono-alumnos.png" style="height: 254px; width: 234px;" class="icono-alumnos" alt="App Alumnos">
						 <p class="titulo-app">APP <br>Alumnos</p></a>
					 </div>
					 
				</div>
				 
			 </div>
			 <div class="instalacion-app-facilitador">
				 <img src="templates/frontend/images/novedades-blackboard/flecha-alumnos.png" class="flecha-app" alt="flecha hacia abajo" style="width: 14px; height: 37px;">
			 	<a class="boton-app-alumnos" href="instalacion-appalumnos.php">Instalación<br>de la APP</a>
			 </div>
			</div>
				 </div>
		 </section>
	 </div>
	</section>
      
      <!-- Final del contenido de la página-->
      <?Php
require_once 'footer/footer_v1.php';
?>
</body>
</html>