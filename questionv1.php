<!DOCTYPE html>
<html lang="en">
<?Php
$faq=base64_encode('faq');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
//echo base64_decode($_GET[categoria]);
switch (base64_decode($_GET[categoria])) {
	case 9:
		$header='single-page-header-faq-moodle';
		$categoria='Moodle';
		break;
	case 10:
		$header='single-page-header-faq-blackboard';
		$categoria='Blackboard';
		break;
	case 11:
		$header='single-page-header-faq-alumnosb';
		$categoria='Alumnos - BUAP';
		break;
}
?>
<?Php 
  $obj = new Database(DB_HOST,DB_KAYAKO,USER_KAYAKO,PASS_KAYAKO);
  $sql='SELECT 
	    	subject, dateline, contents, fullname
		FROM 
		  	swkbarticles, swkbarticledata, swstaff 
		WHERE 
		  	swkbarticles.kbarticleid = swkbarticledata.kbarticleid AND
		  	swkbarticles.creatorid=swstaff.staffid AND
		  	swkbarticles.kbarticleid=:id ';
  $obj->query($sql);				  
  $obj->bind(':id', base64_decode($_GET['articulo']));
  $article=$obj->resultset();
  $obj->null;
  //print_r($article);
  date_default_timezone_set ('America/Mexico_City');
 
  ?>
<!-- Start single page header -->
  <section id="<?Php echo $header;?>">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Preguntas frecuentes / <?Php echo $categoria;?></h2>
              <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>-->
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php?option=<?Php echo base64_encode('inicio');?>">Inicio</a></li>
                <li><a href="faq.php?option=<?Php echo base64_encode('faq'); ?>">Preguntas frecuentes</a></li>
                <li><a href="#"><?Php echo $categoria;?></a></li>
                <li class="active"> <?Php echo utf8_encode($article[0]['subject']);?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End single page header -->
  <!-- Start blog archive -->
  <section id="blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="blog-archive-area">
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-8">
                <div class="blog-archive-left">
                  <!-- Start blog news single -->
                  <article class="blog-news-single">
                    
                    <div class="blog-news-title">
                      <h2><?Php echo utf8_encode($article[0]['subject']);?></h2>
                      <p>Publicado por <a class="blog-author" href="#"><?Php echo utf8_encode($article[0]['fullname']);?></a> <span class="blog-date"><?Php echo date('Y-m-d h:i:s a', $article[0]['dateline']);?></span></p>
                    </div>
                    <div class="blog-news-details blog-single-details">
                      <?Php echo $article[0]['contents'];?>
                      <div class="blog-single-bottom">
                        <div class="row">
                          <div class="col-md-8">
                            <div class="blog-single-tag">
                              <span class="fa fa-tags"></span>
                              <a href="#">Design,</a>
                              <a href="#">Photoshop,</a>
                              <a href="#">Development</a>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="blog-single-social">
                              <a href="#"><i class="fa fa-facebook"></i></a>
                              <a href="#"><i class="fa fa-twitter"></i></a>
                              <a href="#"><i class="fa fa-linkedin"></i></a>
                              <a href="#"><i class="fa fa-google-plus"></i></a>
                              <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </article>
                  <!-- Start blog navigation -->
                  <div class="blog-navigation-area">
                    <div class="blog-navigation-prev">
                      <a href="#">
                        <h5>All about writing story</h5>
                        <span>Previous Post</span>
                      </a>
                    </div>
                    <div class="blog-navigation-next">
                      <a href="#">
                        <h5>All about friends story</h5>
                        <span>Next Post</span>
                      </a>
                    </div>
                  </div>
                  <!-- Start Comment box -->
                  <div class="comments-box-area">
                    <h2>Deja tu un comentario</h2>
                    <p>Su dirección de correo electrónico no será publicada.</p>
                    <form action="" class="comments-form">
                       <div class="form-group">                        
                        <input type="text" class="form-control" placeholder="Tu nombre">
                      </div>
                      <div class="form-group">                        
                        <input type="email" class="form-control" placeholder="Correo electrónico">
                      </div>
                       <div class="form-group">                        
                        <textarea placeholder="Comentario" rows="3" class="form-control"></textarea>
                      </div>
                      <button class="comment-btn">Enviar comentario</button>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-2"></div>             
            </div>
          </div>
        </div>
      </div>
    </div>  
  </section>
  <!-- End blog archive -->
<?Php
require_once 'footer/footer.php';
?>
</body>
</html