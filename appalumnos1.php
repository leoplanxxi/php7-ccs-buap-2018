<!DOCTYPE html>
<html lang="es">
<?Php
$novedades=base64_encode('novedadesbb');
$option=base64_encode('novedadesbb');
define('APLICATION', 'aplication');
require_once APLICATION.'/config.php';
require_once APLICATION.'/seguridadgp.php';
require_once (APLICATION.'/conexion.php');
require_once 'header/header.php';
require_once 'menu/menu.php';
?>
<!--	<link rel="stylesheet" href="<?Php echo DEFAULT_LAYOUT;?>/css/lista-desplegable.css">-->
<link rel="stylesheet" href="video.css">
<script src='js/zoomv1/jquery-1.8.3.min.js'></script>
<script src='js/zoomv1/jquery.elevatezoom.js'></script>


	<link rel="stylesheet" href="style_tabs_acordeon_alumnos.css">

	<script src="js/index.js"></script>


<!-- Start single page header -->
  <section id="single-page-header-novedades">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-left">
              <h2>Novedades Bb / APP Alumnos</h2>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="single-page-header-right">
              <ol class="breadcrumb">
                <li><a href="index.php">Inicio</a></li>
                <li class="active"><a href="novedades_bb.php">Novedades Bb</a></li>
				<li class="active">APP Alumnos</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- End single page header -->

<!-- Inicio del contenido de la página-->
  <section id="contact">
     <div class="container">
       <div class="row">
		   <div class="col-xs-12 col-md-4 div-app-facilitadores">

		   		<img src="templates/frontend/images/novedades-blackboard/icono-facilitadores.png" class="icono-facilitadores" alt="App Facilitadores">
				<p class="titulo-app-fac">APP <br>Alumnos</p>
		 </div>
		   <div class="col-md-2"></div>
         <div class="col-xs-12 col-md-6">
           <div class="title-area title-area-borde">
				 <h1 class="title">Novedades</h1>
				 <h2 class="subtitulo">Plataforma Instruccional Institucional</h2>
			   <center>
			   		<img src="templates/frontend/images/novedades-blackboard/logotipo-blackboard.png" style="height:auto;" class="logo-blackboard" alt="Blackboard">
			   </center>
            </div>
         </div>


       </div>

		<div class="row">
         <div class="col-xs-12 col-md-12">
				<div class="naccs lista-opciones">
				  <div class="grid">
				   <div class="gc col-xs-12 col-md-4">
				    <div class="menu">
				     <div class="active"><span class="light"></span><span>Actividades</span></div>
				     <div><span class="light"></span><span>Cursos</span></div>
				     <div><span class="light"></span><span>Foros</span></div>
						<div><span class="light"></span><span>Notificaciones</span></div>
						<div><span class="light"></span><span>Exámenes</span></div>
						<div><span class="light"></span><span>Calificaciones</span></div>
						<div class="menu-collaborate"><span class="light menu-collaborate"></span><span>Collaborate</span></div>
				    </div>
				   </div>
					  <div class="col-md-2"></div>
				   <div class="gc col-xs-12 col-md-6">
				    <ul class="nacc" style="height: 650px;">
				     <li class="active">
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Contar con su listado de cursos.</p>
						  <p class="novedades-texto-contenido int">Con esta función el facilitador puede visualizar todos los cursos por periodo.</p>

								<img id="zoom_05" src='templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg' width="25%" height="auto" data-zoom-image="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg"/>
				      </div>
				     </li>
				     <li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Obtener una vista preliminar de elementos, actividades y pruebas.</p>
							<span class='zoom' id='ex2'>
				       			<img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
						 	</span>
				      </div>
				     </li>
						<li>
							<div class="contenedor-acordeon"  >
								<p class="novedades-texto-contenido titulo-acordeon">Participar en debates</p>
								<!--<iframe class="iframe-media" width="535px" height="600px" src="https://www.youtube.com/embed/mVdg678ZAC8" frameborder="0" gesture="media" allowfullscreen></iframe>-->

								<div class="container1">
<iframe class="video" src="//www.youtube.com/embed/mVdg678ZAC8" width="600" height="300" frameborder="0" allowfullscreen="allowfullscreen">
</iframe>
</div
						</li>
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Enviar anuncios.</p>
							<span class='zoom' id='ex3'>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
						 </span>
				      </div>
				     </li>
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Generar contenido.</p>
							<span class='zoom' id='ex4'>
				       <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
						 </span>
				      </div>
				     </li>
					<li>
				      <div class="contenedor-acordeon">
						  <p class="novedades-texto-contenido titulo-acordeon">Interactuar con tu clase en<br><span>Blackboard Collaborate.</span></p>
							<span class='zoom' id='ex5'>
							 <img src="templates/frontend/images/novedades-blackboard/facilitador-novedades-1.jpg">
						 </span>
				      </div>
				     </li>
				    </ul>
				   </div>
				  </div>
				 </div>
         </div>
       </div>

     </div>
  </section>

<!-- Final del contenido de la página-->
      <?Php
require_once 'footer/footer_v1.php';
?>

</body>
</html>
<script>
$("#zoom_05").elevateZoom({
  zoomType				: "inner",
  cursor: "crosshair"
});
</script>
